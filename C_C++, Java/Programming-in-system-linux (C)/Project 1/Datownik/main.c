#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <signal.h>
#include <fcntl.h>
#include <string.h>

#define MAX_PATH 30
#define MAX_FIFOS 10


float rand_space(float mean, float deviation)                           // function calculating rand space beetween sending time stemples
{
    float min = mean-deviation;
    float max = mean + deviation;
    return min + (((float) rand()) / (float) RAND_MAX) * (max - min);
}

void sleep_space(float space)                                           // function responsible for waiting calculated time space
{
    struct timespec delay;
    double calk;
    delay.tv_nsec = modf(space, &calk)*1000000000L;
    delay.tv_sec = calk;
    nanosleep(&delay, NULL);
}

int open_fifo(char **fifo_f, int counter_f, int* fd_tab)                // function opening fifos from paths in fifo_f table
{
    int out_fd;
    int i = 0;
    int j = 0;
    for (i = 0, j=0 ; i < counter_f; i++, j++)
    {
        if ((out_fd = open(fifo_f[i], O_WRONLY)) == -1)
        {
            fprintf(stderr, "fifo number %i open error\n", i);
            j--;
        } else
            fd_tab[j] = out_fd;                                         // table with corrctly opened descriptors
    }
    return j;
}

void remove_element(int *array, int index, int array_length)            // function removing and shifting elements in descriptor table
{
    int i;
    for(i = index; i < array_length - 1; i++) array[i] = array[i + 1];
}


void free_memory(char **fifo_tab, int *fd_tab, int counter, char* str )                     // function free all allocated memory and close all opened fifos
{
    int i = 0;
    for (i = 0; i < MAX_FIFOS; i++)
    {
        free(fifo_tab[i]);
    }
    free(fifo_tab);
    free(str);

    int j = 0;
    for (j = 0; j < counter; j++)
        close(fd_tab[j]);

}

void handler(int sig, siginfo_t *si, void *uc)                          // handler using to catching signal from timer and terminating program;
{
    signal(sig, SIG_IGN);
    static const char msg[] = "\nTime ran out, program closed\n";
    write(1, msg, sizeof(msg) - 1);
    exit(0);
}

void loop_timer(float clock, clockid_t cloc_type)                       // timer used to terminate program after time added in one of parameters c,w,p
{
    timer_t timerid;
    struct sigevent sev;
    sev.sigev_notify = SIGEV_SIGNAL;
    sev.sigev_signo = SIGRTMIN;
    sev.sigev_value.sival_ptr = &timerid;

    if (timer_create(cloc_type, &sev, &timerid) == -1)
        fprintf(stderr, "timer_create error\n");

    struct itimerspec its;
    double calk;
    its.it_value.tv_nsec = modf(clock, &calk)*1000000000;
    its.it_value.tv_sec = calk;
    its.it_interval.tv_sec = 0;
    its.it_interval.tv_nsec = 0;

    if (timer_settime(timerid, 0, &its, NULL) == -1)
        fprintf(stderr, "timer_settime error\n");

    struct sigaction sa;
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = handler;
    sigemptyset(&sa.sa_mask);

    if (sigaction(SIGRTMIN, &sa, NULL) == -1)
        fprintf(stderr, "sigaction error\n");
}

void signal_callback_handler(int signum)                                // handler for SIGPIPE signal
{
    fprintf(stderr,"Caught signal SIGPIPE fifo will be closed\n");
}



int main(int argc, char **argv) {


    srand(time(NULL));

    float mean = -1;
    float deviation = 0;

    int opt;
    int real_clock_flag = 0;
    int mon_clock_flag = 0;
    int process_clock_flag = 0;
    float clock_time = 0;

    char *string = (char *) malloc(MAX_PATH);
    char **fifo_f = (char **) malloc(MAX_FIFOS * sizeof(char *));       //allocating memory for fifo paths from -f argumets
    int fifo_s[MAX_FIFOS];                                              // table of fifo descriptors from -s parameter
    int counter_f = 0;
    int counter_s = 0;
    __clockid_t clock_desc;
    opterr = 0;
    struct timespec real_time;
    size_t size = sizeof(struct timespec);
    int fd_tab[MAX_FIFOS];
    int opened_fifos;

    while ((opt = getopt(argc, argv, "m:d:w:c:p:f:s:")) != -1)
    {
        switch (opt)
        {
            case 'm':
                mean = atof(optarg);
                break;
            case 'd':
                deviation = atof(optarg);
                break;
            case 'w':
                real_clock_flag = 1;
                clock_time = atof(optarg);
                clock_desc = CLOCK_REALTIME;
                break;
            case 'c':
                mon_clock_flag = 1;
                clock_time = atof(optarg);
                clock_desc = CLOCK_MONOTONIC;
                break;
            case 'p':
                process_clock_flag = 1;
                clock_time = atof(optarg);
                clock_desc = CLOCK_PROCESS_CPUTIME_ID;
                break;
            case 'f':
                string = strdup(optarg);
                fifo_f[counter_f] = strdup(string);
                counter_f++;
                break;
            case 's':
                fifo_s[counter_s] = atoi(optarg);
                counter_s++;
                break;
            case '?':
                if (optopt == 'm')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else if (optopt == 'd')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else if (optopt == 'w')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else if (optopt == 'c')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else if (optopt == 'p')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else if (optopt == 'f')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else if (optopt == 's')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else if (isprint(optopt))
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
                free_memory(fifo_f,fd_tab, counter_f,string);
                return 1;
            default:
                abort();
                free_memory(fifo_f,fd_tab, counter_f,string);

        }
    }
    if (mean == -1 || mean <= 0)
    {
        fprintf(stderr, "-m is mandatory and must be greater than 0\n");
        free_memory(fifo_f,fd_tab, counter_f,string);
        exit(1);
    }

    if (mean - deviation <= 0)
    {
        fprintf(stderr, "diffrence between mean and deviation must be greater than 0\n");
        free_memory(fifo_f,fd_tab, counter_f,string);
        exit(1);
    }

    if ((real_clock_flag == 1 && (mon_clock_flag == 1 || process_clock_flag == 1)) ||                       // only one clock argument is possible
        (mon_clock_flag == 1 && process_clock_flag == 1))
    {
        fprintf(stderr, "you can chose only one type of clock\n");
        free_memory(fifo_f,fd_tab, counter_f,string);
        exit(1);
    }



    opened_fifos = open_fifo(fifo_f,counter_f, fd_tab);                                                      // opening fifos
    int l = 0;
    int m = 0;
    int res = 0;

    float sleep_time = rand_space(mean, deviation);                                                          // calculating sleep time beetween sending stemples

    if ((opened_fifos > 0) && (counter_s == 0))                                                              // in case only parameter -f is defined (additional information not sending to stdout -f -s)
    {
        printf("mean: %f\n", mean);

        printf("deviation: %f\n", deviation);
        if (clock_time != 0)
            printf("program time: %f\n", clock_time);
        printf("time stempel space %f \n", sleep_time);
        if (opened_fifos > 0)
            printf("number of successfully opened fifos in sender %i\n", opened_fifos);
        else if (opened_fifos < 0 && counter_s == 0) {
            fprintf(stderr, "number of successfully opened fifos in sender %i\n", opened_fifos);
            free_memory(fifo_f,fd_tab, counter_f,string);
            exit(1);
        }
        printf("--------------------------------------------------------------\n");
    }

    if (clock_time>0)
        loop_timer(clock_time, clock_desc);                                                                 // starting program terminating timer


    while(1)
    {
        clock_gettime(CLOCK_REALTIME, &real_time);                                                           // getting real time
        for (l = 0; l < opened_fifos; l++)
        {                                                                                                    // writing to fifos one by one
            if ((res = write(fd_tab[l], &real_time, size)) <= 0)                                             // if nothing was write or received error fifo closing and removing from loop
            {
                fprintf(stderr, "Fifo number %i cannot be write\n", l);
                close(fd_tab[l]);
                remove_element(fd_tab, l, opened_fifos);
                opened_fifos--;
                continue;
            }
        }
        for (m = 0; m < counter_s; m++)                                                                      // writing to fifos like stdout stderr
        {
            write(fifo_s[m], &real_time, size);
        }
        if ((opened_fifos <= 0) && (counter_s <= 0))                                                         // closing program when no fifos to write
        {
            fprintf(stderr, "No fifos to write\n");
            free_memory(fifo_f, fd_tab, counter_f, string);
            break;
        }
        sleep_space(sleep_time);                                                                            // waiting time calculated from parameters -m and -d
    }

    return 0;
}