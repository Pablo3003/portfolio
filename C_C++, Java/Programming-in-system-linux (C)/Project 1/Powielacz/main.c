#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>

#define MAX_PATH 30


void remove_element(int *array, int index, int array_length)            // function removing and shifting elements in descriptor table
{
    int i;
    for(i = index; i < array_length - 1; i++) array[i] = array[i + 1];
}

void signal_callback_handler(int signum)                                // handler for SIGPIPE signal
{
    fprintf(stderr,"Caught signal SIGPIPE fifo will be closed\n");
}

void free_memory(char **fifo_tab, int *fd_tab, int counter, char* path, char* error_path,int max_fifos ) // function free all allocated memory and close all opened fifos
{
    int i = 0;
    for (i = 0; i < max_fifos; i++)
    {
        free(fifo_tab[i]);
    }
    free(fifo_tab);
    free(path);
    free(error_path);

    int j = 0;
    for (j = 0; j < counter; j++)
        close(fd_tab[j]);

}

int main(int argc, char **argv) {

    int opt;
    int max_fifos = 0;
    char* path = (char *) malloc(MAX_PATH);
    char* error_path = (char *) malloc(MAX_PATH);
    struct timespec real_time;
    size_t size = sizeof(struct timespec);
    int fd[max_fifos];                                                                              // descriptor table
    int error_flag = 0;
    int correct_fifo_counter = 0;

    while ((opt = getopt(argc, argv, "p:c:L:")) != -1)
    {
        switch (opt)
        {
            case 'p':
                path = strdup(optarg);
                break;
            case 'c':
                max_fifos = atoi(optarg);
                break;
            case 'L':
                error_path = strdup(optarg);
                error_flag = 1;
                break;
            case '?':
                if (optopt == 'p')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else if (optopt == 'c')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else if (optopt == 'L')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else
                    fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
                return 1;
            default:
                abort();
        }

    }

    char** fifo_f = (char **) malloc(max_fifos * sizeof(char *));                                   // allocating memory for path table

    int error_file;

    if (error_flag == 1)
    {
        if ((error_file = open(error_path,O_WRONLY)) <= 0)
            fprintf(stderr, "Cannot open file to redirect diagnostic information \n");
        else
            dup2(error_file,2);                                                                    // if defined path to error file redirection from stderr
    }

    if(max_fifos <= 0)                                                                             // closing program when no parameter -c
    {
        fprintf(stderr, "Number of fifos in parameter -c should be greater than 0 \n");
        free_memory(fifo_f,fd,correct_fifo_counter,path,error_path, max_fifos);
        exit(1);
    }

    int j = 0;
    struct stat buf;
    int temp;

    struct timespec delay;                                                                          // struct for nanosleep
    delay.tv_nsec = 100;
    delay.tv_sec = 0;
    for (j = 0; j < max_fifos; j++)
    {
        asprintf(&fifo_f[j], "%s%d",path, j);                                                       // contactenation path wit numbers from parameter -c asprintf allocating memory
        if ( stat(fifo_f[j], &buf) == 0 )                                                           // checking if file is open for reading
        {
            if (S_ISFIFO(buf.st_mode) == 1)                                                         // checking if file is a fifo
            {
                if ((temp = open(fifo_f[j], O_WRONLY | O_NONBLOCK)) > 0)                            // opening fifo
                {
                    nanosleep(&delay, NULL);
                    fd[correct_fifo_counter] = temp;                                                // table of only correct fifos
                    correct_fifo_counter++;
                    //printf("fifo %s is correct\n", fifo_f[j]);
                    continue;     
                }
            }
        }
        //printf("fifo %s is incorrect\n", fifo_f[j]);
    }
    if(correct_fifo_counter <= 0)                                                                   // if no correctly opened fifos closing program
    {
        fprintf(stderr, "No readers\n");
        free_memory(fifo_f,fd,correct_fifo_counter,path,error_path, max_fifos);
        exit(1);
    }

    int m = 0;
    int res = 0;
    signal(SIGPIPE, signal_callback_handler);                                                       // SIGPIPE catching
    while(read(STDIN_FILENO, &real_time, size))                                                     // reading while is something in stdin
    {
        printf("Time stemple: %llds  %ldns \n", (long long)real_time.tv_sec, real_time.tv_nsec);    // printing time stemple
        for (m = 0; m < correct_fifo_counter; m++)                                                  // writing one by one to all fifos
        {
            if((res = write(fd[m], &real_time, size)) <= 0)                                         // if nothing was write or received error fifo closing and removing from loop
            {
                fprintf(stderr, "Fifo number %i cannot be write\n", m);
                close(fd[m]);
                remove_element(fd,m,correct_fifo_counter);
                correct_fifo_counter--;
                continue;
            }
            if (res != size )                                                                        // in case of fifo overloaded fifo removing from loop and closing
            {
                fprintf(stderr, "Fifo number %i overloaded\n", m);
                close(fd[m]);
                remove_element(fd,m,correct_fifo_counter);
                correct_fifo_counter--;
            }
        }
        if (correct_fifo_counter == 0 )                                                             // when all fifos closed, closing program
        {
            fprintf(stderr, "No fifos to write\n");
            break;
        }
    }
    free_memory(fifo_f,fd,correct_fifo_counter,path,error_path, max_fifos);
    return 0;
}