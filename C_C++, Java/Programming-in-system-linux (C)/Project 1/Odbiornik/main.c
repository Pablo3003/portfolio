#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <signal.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>

#define  MAX_PATH 30
#define  MAX_FIFOS 10


int open_fifo(char **fifo_f, int counter_f, int* fd_tab)                                    // function opening fifos and returning number of succsessfully opened fifos
{
    int out_fd;
    int i = 0;
    int j = 0;
    for (i = 0; i < counter_f; i++)
    {
        if ((out_fd = open(fifo_f[i], O_RDONLY)) == -1)
        {
            fprintf(stderr, "fifo number %i open error\n", i);
            j--;
        } else
            fd_tab[j] = out_fd;                                                             //descriptors table
        j++;
    }
    return j;
}

void free_memory(char **fifo_tab, int *fd_tab, int counter, char* str )                     // function free all allocated memory and close all opened fifos
{
    int i = 0;
    for (i = 0; i < MAX_FIFOS; i++)
    {
        free(fifo_tab[i]);
    }
    free(fifo_tab);
    free(str);

    int j = 0;
    for (j = 0; j < counter; j++)
        close(fd_tab[j]);

}

void timer_handler(int signum)                                                              // timer handler to write message with intervals defined in -d parameter
{
    static const char msg[] = "\n-----------I am still alive-----------\n\n";
    write(1, msg, sizeof(msg));
}

void loop_timer(float clock)                                                                // timer function taking number of seconds defined in -d parameter
{
    struct sigaction sa;
    struct itimerval timer ;

    memset ( &sa, 0, sizeof ( sa ) ) ;

    sa.sa_handler = &timer_handler ;
    sigaction ( SIGALRM, &sa, NULL );

    double calk;
    timer.it_value.tv_usec = modf(clock, &calk) * 1000000;
    timer.it_value.tv_sec = calk ;
    timer.it_interval.tv_usec = modf(clock, &calk) * 1000000;
    timer.it_interval.tv_sec = calk;

    setitimer ( ITIMER_REAL, &timer, NULL ) ;
}

void remove_element(int *array, int index, int array_length)                                // function used to removing and shifting descriptors after some fifo was closed
{
    int i;
    for(i = index; i < array_length - 1; i++)
        array[i] = array[i + 1];
}

void timespec_diff(struct timespec *start, struct timespec *stop, struct timespec *result)  // function calculating diffrence beetween timespec structures
{
    if ((stop->tv_nsec - start->tv_nsec) < 0)
    {
        result->tv_sec = stop->tv_sec - start->tv_sec - 1;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
    } else
    {
        result->tv_sec = stop->tv_sec - start->tv_sec;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec;
    }

    return;
}

int main(int argc, char **argv)
{
    int opt;
    double clock_time = 0;
    int counter_f = 0;
    int opened_fifos = 0;
    int fd_tab[MAX_FIFOS];                                                                  // table of descriptors

    char *str = (char *) malloc(MAX_PATH);                                                  // memory for 1 string (path)
    char **fifo_f = (char **) malloc(MAX_FIFOS * sizeof(char *));                           // allocating memory for fifo paths
    struct timespec actual_time;
    size_t size = sizeof(struct timespec);
    struct timespec received_time;
    struct timespec diff;

    while ((opt = getopt(argc, argv, "d:")) != -1)
    {
        switch (opt)
        {
            case 'd':
                clock_time = atof(optarg);
                break;
            case '?':
                if (optopt == 'd')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else if (optopt == 'f')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else
                    fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
                free_memory(fifo_f, fd_tab, opened_fifos, str);
                return 1;
            default:
                free_memory(fifo_f, fd_tab, opened_fifos, str);
                abort();
        }
    }

    int index;
    for (index = optind; index < argc; index++)                                              // loop reading paths whithout argument
    {
        str = strdup(argv[index]);
        fifo_f[counter_f] = strdup(str);
        counter_f++;
    }

    if(counter_f == 0)                                                                      // when no fifo opened closing program
    {
        fprintf(stderr, "No fifos added by parameter \n");
        free_memory(fifo_f, fd_tab, opened_fifos, str);
        exit(1);
    }

    if (clock_time < 0)                                                                     // when parameter -d defined whithout value
    {
        fprintf(stderr, "Value of -d parameter should be greater than 0\n");
        free_memory(fifo_f, fd_tab, opened_fifos, str);
        exit(1);
    }


    int j = 0;
    opened_fifos = open_fifo(fifo_f,counter_f, fd_tab);                                     // opening fifos

    printf("\nNumber of successfully opened fifo in receiver: %i \n", opened_fifos);
    printf("--------------------------------------------------------\n");

    loop_timer(clock_time);                                                                 // timer for "still alive" communication

    while(opened_fifos > 0)
    {
        for (j = 0; j < opened_fifos; j++)                                                  // reading from fifos one by one
        {
            if(!read(fd_tab[j], &received_time, size))                                      // if nothing was read closing and removing fifo from loop
            {
                remove_element(fd_tab,j,opened_fifos);
                fprintf(stderr, "fifo number %i was closed as any information received\n", j);
                opened_fifos--;
                if (opened_fifos == 0)                                                       // if las fifo was closed exit from program
                {
                    fprintf(stderr, "No opened fifos\n");
                    free_memory(fifo_f, fd_tab, opened_fifos, str);
                    exit(1);
                }
                continue;
            }
            printf("fifo number %i: time stempel %llds  %ldns \n", j, (long long)received_time.tv_sec, received_time.tv_nsec);
            clock_gettime(CLOCK_REALTIME, &actual_time);
            printf("Actual real time: %llds  %ldns \n", (long long)actual_time.tv_sec, actual_time.tv_nsec);
            timespec_diff(&received_time, &actual_time, &diff);
            printf("Diffrence beetween actual and received time %llds  %ldns\n\n", (long long)diff.tv_sec, diff.tv_nsec);       // report
        }
        printf("*****************************************************\n");
    }

    free_memory(fifo_f, fd_tab, opened_fifos, str);

    return 0;
}