Polecenie do zadania otrzymane od prowadz�cego:


Skr�t. Celem jest zestaw trzech wsp�pracuj�cych program�w:

- powielacz - program g��wny, kt�ry kopiuje swoje standardowe wej�cie na wyznaczone wyj�cia,

- datownik - wysy�aj�cy na standardowe wyj�cie warto�� stempla czasowego,

- odbiornik - odczytuj�cy stemple i wy�wietlaj�cy raporty,

oraz skrypty prezentuj�ce ciekawsze scenariusze ich dzia�ania.

1. Datownik

Program odczytuj�cy warto�ci zegara czasu realnego i wysy�aj�cy je wskazanymi kana�ami

komunikacyjnymi.

Parametry, przyjmowane przez program.

1. Odst�py mi�dzy odczytami s� losowane, a parametry losowania s� okre�lone przez:

- -m <float> - �rednia warto�� odst�p�w (parametr obowi�zkowy),

- -d <float> - odchylenie od warto�ci �redniej (opcjonalny z warto�ci� domy�ln� 0).

Oba parametry s� podawane w sekundach.

2. Czas dzia�ania jest opisywany przez co najwy�ej jeden z poni�szych parametr�w. Brak

podania jakiegokolwiek z nich oznacza dzia�anie w niesko�czono��.

Poszczeg�lne parametry okre�laj�, w.g. kt�rego z zegar�w ma by� mierzony czas, i przyjmuj�

warto�� oznaczaj�c� liczb� sekund.

- -w <float> - zegara czasu realnego,

- -c <float> - monotoniczny,

- -p <float> - lokalny procesu.

3. Kana�y komunikacyjne okre�laj�, gdzie informacja o warto�ci zegara ma by� przekazywana.

Parametry mog� si� wielokrotnie powtarza�, ale wymagany jest co najmniej jeden.

- -f <string>,

- -s <int>.

Opis poni�ej.

Dzia�anie. Co losowy odst�p czasu (losowanie z zakresu okre�lonego przez parametry -m, -d)

pobierany jest czas z zegara realnego i wysy�any do wszystkich kana��w okre�lonych parametrami

-f i -s.

Je�eli by� podany kt�ry� z parametr�w -w, -c, -p, to program ko�czy dzia�anie po okresie

odliczonym przez w�a�ciwy zegar.

Kana�y, z kt�rych mo�e korzysta� program, to kolejki FIFO (-f), kt�re musi sam otworzy�, albo

otwarte pliki odziedziczone po rodzicu (-s). W tym drugim przypadku identyfikacja, z kt�rego

pliku ma korzysta� odbywa si� przez podanie numeru jego deskryptora.

2. Odbiornik

Odczytuje zawarto�� kolejki, por�wnuje z bie��cym stanem zegara i wypisuje linijk� raportu.

Parametry:

- -d <float> - opcjonalny, okre�la okres, po kt�rym generuje komunikat, �e nadal dzia�a (ze

stemplem czasowym),

- <string> - �cie�ka, pod kt�r� ma szuka� pliku FIFO.

Dzia�anie. Proces stara si� odczytywa� na bie��co nadchodz�ce dane. Ka�dy odczyt obejmuje

warto�� jednego stempla, wys�anego przez proces datownika. Odczytane dane s� por�wnywane

z bie��co odczytanym stanem zegara, a uzyskane informacje s� prezentowane na standardowym

wyj�ciu.

Dodatkowo, je�eli zosta� przekazany parametr -d, to w ustalonych odst�pach czasu jest wy-
sy�any na standardowe wyj�cie komunikat z bie��cym stanem lokalnego zegara procesu.

3. Powielacz (uproszczony)

Proces powielacza kopiuje sw�j strumie� standardowy do obs�ugiwanych plik�w FIFO. Ko-
piowanie ma si� odbywa� z mo�liwie jak najmniejszymi op�nieniami (b�d� przesy�ane stemple

czasowe). Proces ma by� odporny na zdarzenia zamkni�cia lub przepe�nienie kolejki.

Parametry przyjmowane przez program:

- -p <string> - wzorzec nazw plik�w,

- -c <int> - liczba obs�ugiwanych kolejek.

- -L <string> - parametr opcjonalny, ze �cie�k�, do kt�rej maj� trafia� informacje diagno-
styczne (bez parametru nie s� gromadzone).

�cie�ki do plik�w FIFO powstaj� przez doklejanie do wzorca kolejnych liczb, od 1 do warto�ci

podanej w parametrze.

Procedura. Przed przyst�pieniem do w�a�ciwej dzia�alno�ci powielacz sprawdza, kt�re z plik�w

o zadanych �cie�kach s� kolejkami FIFO i s� otwarte do czytania (przez potencjalnych odbiorc�w).

Je�eli w trakcie dzia�ania jakie� strumienie zostan� ca�kowicie zamkni�te (przez czytaj�cych),

to powielacz wyrzuca je z listy obs�ugiwanych. Podobnie, gdy kt�ry� z kana��w zostanie przepe�-

niony, to zostaje karnie przez powielacz zamkni�ty i wyrzucony.

Je�eli wyrzucone zosta�y wszystkie strumienie (albo, gdy �aden nie zosta� otworzony) powie-
lacz nie ma powod�w by dalej dzia�a�.
