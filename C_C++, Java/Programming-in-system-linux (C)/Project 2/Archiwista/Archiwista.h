#ifndef ARCHIWISTA_ARCHIWISTA_H
#define ARCHIWISTA_ARCHIWISTA_H

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <openssl/md5.h>
#include <math.h>
#include <pthread.h>
#include <fcntl.h>
#include <signal.h>



#define MAX_MESSAGE 100
#define ADDRLEN 6
#define MAX_ID 100

extern pid_t pid;                                  // global variables used by all more than one thread
extern struct Identifier id;
extern char* address_p;
extern int fdClient;
extern char** w_names;
extern int fd_tab[MAX_ID];
extern int indicator;
extern char* out_str;
extern int Md5check;

struct Identifier
{
    int workers;
    char name[MAX_ID];
    ssize_t msg_len;
    pid_t boss_pid;
    char md5[33];
};

struct SampleMessage
{
    char byte;
    struct timespec real_time;
};

struct SortedMessage
{
    char byte;
    unsigned long long mili;
};

void* registration(void* unused);
void* archiver(void* unused);
char *randstring(size_t length);
char* private_channel(char *str, int* fd, struct sockaddr_un* serv_addr);
char** worker_channels(char** pcSocketName, int* socket_fd, struct sockaddr_un* serv_addr, struct Identifier ide);
void join_message(struct SortedMessage* msg, int size,  char* str);
void timespec_to_microsec(struct SampleMessage* msg, int size,struct SortedMessage* srt);
int partition(struct SortedMessage* tab, int p, int r);
void quicksort(struct SortedMessage* tab, int p, int r);
void md5sum(char* str, char* out, int size);


#endif //ARCHIWISTA_ARCHIWISTA_H
