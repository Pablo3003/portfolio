#include "Archiwista.h"

char* address_p = NULL;

int main(int argc, char** argv)
{
    int opt;
    while ((opt = getopt(argc, argv, "a:")) != -1)
    {
        switch (opt) {
            case 'a':
                address_p = strdup(optarg);
                break;
            case '?':
                if (optopt == 'a')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else
                    fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
                return 1;
            default:
                abort();
        }
    }

    if(strlen(address_p) == 0)
    {
        fprintf(stderr, "You need to specify using -m parameter addres to registration\n");
        exit(1);
    }
    pthread_t thread_register;
    pthread_t thread_archiver;

    pthread_create(&thread_register, NULL, &registration, NULL);                                    // thread responsible for listenig register requests and menaging registration

    while (indicator != 0)                                                                          // synchronization element
        sleep(1);

    pthread_create(&thread_archiver, NULL, &archiver, NULL);                                        // thread responsible for receiving and joining message
        int r = 0;
    int exists = 1;
    while(1)                                                                                        // loop checking if brigadist is still alive, when not exit program
    {
        sleep(1);
        exists = kill(pid, 0);
        if (exists != 0)
        {
            for (r = 0; r < id.workers; r++)
            {
                close(fd_tab[r]);
            }
            break;
        }
    }

    free(out_str);
    free(address_p);
    return 0;
}
