#include "Archiwista.h"

pid_t pid;                                  // global variables used by all more than one thread
struct Identifier id;
int fdClient;
char** w_names = NULL;
int fd_tab[MAX_ID];
char* out_str = NULL;
int indicator = 1;
int Md5check = 0;


void* registration(void* unused)                                    // function menaging all register process
{
    char* str = NULL;
    int socket1;
    int socket2;
    int socket_fd;
    socklen_t size;
    ssize_t len;
    struct sockaddr_un Ar_public;
    struct sockaddr_un Br_public;
    struct sockaddr_un serv_addr;

    if ((socket1 = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)          // socket for public communication
    {
        perror("socket");
        exit(1);
    }

    Ar_public.sun_family = AF_UNIX;
    strcpy(Ar_public.sun_path, address_p);
    unlink(Ar_public.sun_path);
    len = strlen(Ar_public.sun_path) + sizeof(Ar_public.sun_family);

    if (bind(socket1, (struct sockaddr *) &Ar_public, len) == -1)
    {
        perror("bind");
        exit(1);
    }
    if (listen(socket1, 5) == -1)
    {
        perror("listen");
        exit(1);
    }

    char* priv_soc = NULL;
    socklen_t addr_len = sizeof(priv_soc);
    while(1)
    {
        if(indicator == 1)                              // simple synchronization beetween threads
        {
            for (;;)                                    // public connection with new brigadist
            {
                printf("Public registration...\n");
                size = sizeof(Br_public);
                if ((socket2 = accept(socket1, (struct sockaddr *) &Br_public, &size)) == -1)
                {
                    perror("accept");
                    exit(1);
                } else if (socket2 > 0)
                {
                    printf("Public connection assigned.\n");
                    break;
                }
            }
            if (read(socket2, &id, sizeof(struct Identifier)) > 0)                          // if public connection assigned and there is some date from Brigadist
            {
                pid = id.boss_pid;
                priv_soc = strdup(private_channel(str, &socket_fd, &serv_addr));            // private socket for brigadist creation
                write(socket2, priv_soc, sizeof(priv_soc) + 2);                             // sending to brigadist information about name of assigning socket for him

                fdClient = accept(socket_fd, (struct sockaddr *) &serv_addr, &addr_len);
                if (0 >= fdClient) {
                    printf("accept() failed: [%d][%s]\n", errno, strerror(errno));
                    exit(1);
                }
                w_names = (char **) malloc(MAX_ID * sizeof(char *));
                struct sockaddr_un w_addr[MAX_ID];
                worker_channels(w_names, fd_tab, w_addr, id);                               // menaging private sockets for workers
                ssize_t n = 0;
                for (int i = 0; i < id.workers; ++i)
                {
                    n = write(fdClient, w_names[i], sizeof(w_names[i]));                    // sending by private socket information about names of private sockets for workers
                    if (0 > n)
                    {
                        printf("recv() failed: [%d][%s]\n", errno, strerror(errno));
                        break;
                    }
                    //printf("name %s\n", w_names[i]);
                }
            }
            indicator = 0;
            sleep(1);
        }
    }
    return NULL;
}

void* archiver(void* unused)                                                                // function responsible for reading and joining data from brigadist
{
    while(1)
    {
        if(indicator == 0)                                                                  // element of synchronisation
        {
            struct sockaddr_un serv_addr2[MAX_ID];
            socklen_t addr_size;
            addr_size = sizeof(sa_family_t) + strlen(w_names[0]) + 1;
            struct SampleMessage buff[MAX_MESSAGE];
            size_t size_b = sizeof(struct SampleMessage);
            int n = 0;
            int m = 0;
            int checkout = 0;
            struct SortedMessage tab[MAX_MESSAGE];
            //printf("message len %i\n",id.msg_len);
            fcntl( fdClient, F_SETFL, fcntl(fdClient, F_GETFL) | O_NONBLOCK);               // adding NON_BLOCKIN to private canale to avoid stoping loop
            while (checkout!=1)                                                               // loop responsible for reading till checkout message from brigadist
            {
                for (int i = 0; i < id.workers; i++)
                {
                    if(checkout == 2)                                                               // if 2 received, Archiwist send md5 chcksum to Bigadist after end of work
                        Md5check = 1;
                    if (i >= id.msg_len)
                        break;
                    if (recvfrom(fd_tab[i], &buff[m], size_b, 0, (struct sockaddr *) &serv_addr2[i], &addr_size) > 0)
                    {
                        printf("stempel numer %i: %c czas %llds  %ldns\n", m, (buff[m]).byte,
                               (long long) (buff[m]).real_time.tv_sec, (buff[m]).real_time.tv_nsec);
                        m++;
                    }
                    else if (0 > n)
                    {
                        printf("recv() failed: [%d][%s]\n", errno, strerror(errno));
                        close(fd_tab[i]);
                        exit(1);
                    }
                }
                read(fdClient, &checkout, sizeof(int));                                             // checking if there is checkout flag from brigadist
            }
            timespec_to_microsec(buff, m, tab);                                                     // changing timespec structure received in time stemple to microsecond used for sorting purpose
            quicksort(tab, 0, m - 1);                                                               // sorting all message part using time stemples
            out_str = (char *) malloc(id.msg_len);
            join_message(tab, m, out_str);                                                          // string concatenation

            printf("Restored message %s\n", out_str);
            char *md5 = (char *) malloc(33);
            md5sum(out_str, md5, m);                                                                //calculating md5 sum of joined data
            printf("New md5 checksum: %s\n", md5);
            write(fdClient,md5,sizeof(md5));
            printf("Old md5 checksum: %s\n", id.md5);
            if (strcmp(md5, id.md5) == 0)                                                           // checking if sum i the same as received from brigadist
                printf("Congratulation: successfully received message\n");
            indicator = 1;                                                                          // element of synchronization
        }
    }
}

char* private_channel(char* pcSocketName, int* socket_fd, struct sockaddr_un *serv_addr)            // function responsible for private channel between brigadist and archiver
{

    int iErr     = 0;

    pcSocketName = randstring(ADDRLEN);                                                             // generating random name of private socket

    memset(serv_addr, 'x', sizeof(*serv_addr));
    (*serv_addr).sun_family = AF_UNIX;
    (*serv_addr).sun_path[0] = '\0';
    strncpy((*serv_addr).sun_path+1, pcSocketName, strlen(pcSocketName));
    *socket_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(-1 == *socket_fd) {
        printf("socket() failed: [%d][%s]\n", errno, strerror(errno));
        exit(1);
    }
    iErr = bind(*socket_fd, (struct sockaddr *)serv_addr, offsetof(struct sockaddr_un, sun_path) + 1/*\0*/ + strlen(pcSocketName));

    if(0 != iErr)
    {
        printf("bind() failed: [%d][%s]\n", errno, strerror(errno));
        exit(1);
    }

    iErr = listen(*socket_fd, 1);
    if(0 != iErr)
    {
        printf("listen() failed: [%d][%s]\n", errno, strerror(errno));
        exit(1);
    }

    return pcSocketName;
}

char** worker_channels(char** pcSocketName, int* socket_fd, struct sockaddr_un* serv_addr, struct Identifier ide)       // menaging sockets for workers
{
    int size = ide.workers;
    int i = 0;
    for (i=0; i<size; i++)
    {
        asprintf(&pcSocketName[i],"%s%d",ide.name,i);                                                                   // names are the same as first private socket but with added numbers
        //printf("%s name %i\n",pcSocketName[i],i);
    }

    int iErr = 0;
    int j = 0;
    for (j=0; j<size; j++)
    {
        memset(serv_addr, 'x', sizeof(*serv_addr));
        (serv_addr[j]).sun_family = AF_UNIX;
        strcpy(&(serv_addr[j]).sun_path[1], pcSocketName[j]);
        (serv_addr[j]).sun_path[0] = '\0';

        socket_fd[j] = socket(AF_UNIX, SOCK_DGRAM, 0);
        if (-1 == socket_fd[j]) {
            printf("socket() failed: [%d][%s]\n", errno, strerror(errno));
            exit(1);
        }

        iErr = bind(socket_fd[j], (struct sockaddr *)&serv_addr[j], sizeof(sa_family_t) + strlen(pcSocketName[j]) + 1);
        if (0 != iErr) {
            printf("bind() failed: [%d][%s]\n", errno, strerror(errno));
            exit(1);
        }
        fcntl( socket_fd[j], F_SETFL, fcntl(socket_fd[j], F_GETFL) | O_NONBLOCK);                                       // all workers channells are in NONNLOCK mode
    }
    return pcSocketName;
}

void timespec_to_microsec(struct SampleMessage* msg, int size,struct SortedMessage* tab)                                // changing timespec to microseconds
{
    int i = 0;
    double sec;
    int milisec;
    double temp;
    for(i=0; i<size; i++)
    {
        sec = modf((double)(msg[i]).real_time.tv_sec/1000,&temp);                                                       // taking only firs 1000 seconds (assumption)
        sec = sec * 1000000000000;                                                                                      // changing to nanoseconds
        milisec = (int)(msg[i]).real_time.tv_nsec;
        tab[i].mili = (unsigned long long)sec + (unsigned long long)milisec;                                            // adding sec and nanosed part
        tab[i].byte = msg[i].byte;
    }
}


int partition(struct SortedMessage* tab, int p, int r)                                                                  // partition for quicksort
{
    unsigned long long int x = tab[p].mili;
    int i = p, j = r;
    struct SortedMessage w;
    while (1)
    {
        while (tab[j].mili > x)
            j--;
        while (tab[i].mili < x)
            i++;
        if (i < j)
        {
            w = tab[i];
            tab[i] = tab[j];
            tab[j] = w;
            i++;
            j--;
        }
        else
            return j;
    }
}

void quicksort(struct SortedMessage* tab, int p, int r)                                                                    // sorting structure using time stemple
{
    int q;
    if (p < r)
    {
        q = partition(tab,p,r);
        quicksort(tab, p, q);
        quicksort(tab, q+1, r);
    }
}

void join_message(struct SortedMessage* msg, int size, char* str)                                                           // contactenating string
{
    char *res = NULL;
    int i = 0;
    for (i = 0; i < size; i++)
    {
        asprintf(&res,"%s%c",str,(msg[i]).byte);
        strcpy(str,res);
    }
    asprintf(&res,"%s%c",str,'\0');
    strcpy(str,res);
}

void md5sum(char* str, char* out, int size)                                                                                 // md5 checksum calculating
{
    int n = 0;
    MD5_CTX c;
    unsigned char digest[16];

    MD5_Init(&c);

    while (size > 0)
    {
        if (size > 512)
        {
            MD5_Update(&c, str, 512);
        } else
        {
            MD5_Update(&c, str, size);
        }
        size -= 512;
        str += 512;
    }

    MD5_Final(digest, &c);

    for (n = 0; n < 16; ++n) {
        snprintf(&(out[n*2]), 16*2, "%02x", (unsigned int)digest[n]);
    }

}

char *randstring(size_t length)                                                                                         // random string generating
{

    srand(time(NULL));
    static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.-#'?!";
    char *randomString = NULL;

    if (length)
    {
        randomString = malloc(sizeof(char) * (length +1));

        if (randomString)
        {
            for (int n = 0;n < length;n++)
            {
                int key = rand() % (int)(sizeof(charset) -1);
                randomString[n] = charset[key];
            }
            randomString[length] = '\0';
        }
    }
    return randomString;
}
