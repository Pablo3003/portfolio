#include "Brygadzista.h"

volatile int g_running = 1;
volatile int md5_check = 0;

int public_connection(char* address)                                                                    // manage public connection
{
    int s;
    size_t len;
    struct sockaddr_un remote;

    if ((s = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
    {
        perror("socket");
        exit(1);
    }
    printf("Trying to connect...\n");

    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, address);
    len = strlen(remote.sun_path) + sizeof(remote.sun_family);
    if (connect(s, (struct sockaddr *)&remote, len) == -1)
    {
        perror("public connect");
        exit(1);
    }
    printf("Public connection assigned.\n");

    return s;
}

char *randstring(size_t length)                                                                         // creating random string used to create sockets in abstract namespace
{
    srand(time(NULL));
    static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.-#'?!";
    char *randomString = NULL;

    if (length)
    {
        randomString = malloc(sizeof(char) * (length +1));

        if (randomString)
        {
            for (int n = 0;n < length;n++)
            {
                int key = rand() % (int)(sizeof(charset) -1);
                randomString[n] = charset[key];
            }
            randomString[length] = '\0';
        }
    }
    return randomString;
}

int private_connection(char* name,char** w_names, int workers, int* w_fd, struct sockaddr_un* w_cont)        // manage all private sockets
{
    int fdClient = 0;
    struct sockaddr_un serv_addr;
    ssize_t iErr    = 0;

    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sun_family = AF_UNIX;
    serv_addr.sun_path[0] = '\0';
    strncpy(serv_addr.sun_path+1, name, strlen(name));

    fdClient = socket(PF_UNIX, SOCK_STREAM, 0);
    if(-1 == fdClient) {
        printf("socket() failed: [%d][%s]\n", errno, strerror(errno));
        exit(1);
    }

    iErr = connect(fdClient, (struct sockaddr*) &serv_addr, offsetof(struct sockaddr_un, sun_path) + 1/*\0*/ + strlen(name));
    if(0 != iErr)
    {
        printf("connect() failed: [%d][%s]\n", errno, strerror(errno));
        exit(1);
    }

    int i = 0;
    for ( i = 0; i < workers; i++)
    {
        iErr = read(fdClient, w_names[i], sizeof(w_names[i]));
        if(0 > iErr)
        {
            printf("read() failed: [%d][%s]\n", errno, strerror(errno));
            exit(1);
        }
    }

    struct sockaddr_un w_addr[MAX_ID];

    int j = 0;
    for (j=0; j<workers; j++)
    {
        memset(&w_addr, 0, sizeof(w_addr));
        (w_addr[j]).sun_family = AF_UNIX;
        strcpy(&(w_addr[j]).sun_path[1], w_names[j]);
        (w_addr[j]).sun_path[0] = '\0';

        w_fd[j] = socket(AF_UNIX, SOCK_DGRAM, 0);
        if (-1 == w_fd[0])
        {
            printf("socket() failed: [%d][%s]\n", errno, strerror(errno));
            exit(1);
        }
        w_cont[j] = w_addr[j];
    }
    return fdClient;
}

void md5sum(char* str, char* out, int size)                         // calculating MD5 checksum of given string
{
    int n = 0;
    MD5_CTX c;
    unsigned char digest[16];

    MD5_Init(&c);

    while (size > 0)
    {
        if (size > 512)
        {
            MD5_Update(&c, str, 512);
        } else
        {
            MD5_Update(&c, str, size);
        }
        size -= 512;
        str += 512;
    }

    MD5_Final(digest, &c);

    for (n = 0; n < 16; ++n)
    {
        snprintf(&(out[n*2]), 16*2, "%02x", (unsigned int)digest[n]);
    }
}

void handle_sigalrm(int signal)                                         // handler informing that received diffrent signal than sigalrm
{
    if (signal != SIGALRM)
    {
        fprintf(stderr, "Caught wrong signal: %d\n", signal);
    }
}
void do_sleep(int seconds)                                              // sleeping until receive SIGALRM
{
    struct sigaction sa;
    sigset_t mask;

    sa.sa_handler = &handle_sigalrm;                                    // Intercept and ignore SIGALRM
    sa.sa_flags = SA_RESETHAND;                                         // Remove the handler after first signal
    sigfillset(&sa.sa_mask);
    sigaction(SIGALRM, &sa, NULL);
                                                                        // Get the current signal mask
    sigprocmask(0, NULL, &mask);                                        // Unblock SIGALRM
    sigdelset(&mask, SIGALRM);
                                                                         // Wait with this mask
    alarm(seconds);
    sigsuspend(&mask);
}

void sig_handler(int signo)                                             // handler for SIGALRM from childrens to parent
{
    if (signo == SIGALRM)
    {
        printf("received SIGALRM that workers done their job\n");
        g_running = 0;
    }
    if (signo == SIGUSR1)
    {
        md5_check = 1;
    }
}

void send_signal (int pid)                                              // childrens send SIGALRM to inform parent that they done work
{
    kill(pid,SIGALRM);
}

