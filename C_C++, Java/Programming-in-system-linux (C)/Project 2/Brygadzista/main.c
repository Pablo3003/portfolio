#include "Brygadzista.h"

int *glob_var;
int *glob_ind;


int main(int argc, char **argv )
{

    int opt = 0;
    int workers = 0;
    char *message = (char*)malloc(MAX);
    char *address_p = (char*)malloc(MAX);
    double clock_time = 0;

    while ((opt = getopt(argc, argv, "w:m:t:a:")) != -1)
    {
        switch (opt) {
            case 'w':
                workers = atoi(optarg);
                break;
            case 'm':
                message = strdup(optarg);
                break;
            case 't':
                clock_time = atof(optarg);
                break;
            case 'a':
                address_p = strdup(optarg);
                break;
            case '?':
                if (optopt == 'w')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                if (optopt == 'm')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                if (optopt == 't')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                if (optopt == 'a')
                    fprintf(stderr, "Option `-%c' requires an argument.\n", optopt);
                else
                    fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
                return 1;
            default:
                abort();
        }
    }

    if(workers == 0)
    {
        fprintf(stderr, "You need to declare number of workers using -w parameter\n");
        exit(1);
    }
    if(clock_time == 0)
    {
        fprintf(stderr, "You need to declare time periods using -t parameter\n");
        exit(1);
    }
    if(strlen(message) == 0)
    {
        fprintf(stderr, "You need to give message to send using -m parameter\n");
        exit(1);
    }
    if(strlen(address_p) == 0)
    {
        fprintf(stderr, "You need to specify using -m parameter addres to registration\n");
        exit(1);
    }



    ssize_t len = strlen(message);
    char* md5 = (char*)malloc(33);
    md5sum(message,md5,len);                                                                            // preparing md5 chhecksum of message for archivist
    printf("md5 checksum %s\n",md5);
    char *str = (char*)malloc(MAX);
    char *randstr = NULL;
    randstr = randstring(ACTUAL_ID-1);                                                                  // generating task name
    printf("Randomize generated identifier of job %s\n",randstr);
    int pub_soc = public_connection(address_p);                                                         // assigning public connection with archiver
    struct Identifier id;                                                                               // preparing identification message
    id.workers = workers;
    strcpy(id.name,randstr);
    id.msg_len = len;
    id.boss_pid = getpid();
    strcpy(id.md5, md5);
    printf("Boss pid %i\n",id.boss_pid);                                                                // Users know parent pid and know whou should get signal to check archiwis md5
    int fd[2];
    pipe(fd);                                                                                           // creating pipe to parent - children communication

    char readbuffer;
    write(pub_soc,&id,sizeof(struct Identifier));                                                       // sending Identifier to archiver with request to register
    read(pub_soc,str,sizeof(str)+2);                                                                    // reading name of socket to private comunication
    struct sockaddr_un w_addr[MAX_ID];
    int w_fd[MAX_ID];
    char** w_names = (char **) malloc(MAX_ID * sizeof(char *));
    int i = 0;
    for ( i = 0; i < workers; i++ )
    {
        w_names[i] = (char*) calloc(50, sizeof(char));
    }

    int fd_P = private_connection(str, w_names, id.workers, w_fd,w_addr);                               // assigning private sockets

    socklen_t addr_size;
    addr_size = sizeof(sa_family_t) + strlen(w_names[0]) + 1;

    struct Message package;
    size_t size_p = sizeof(struct Message);

    glob_var = mmap(NULL, sizeof *glob_var, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0); // mapping variables shared beetwen all processes
    glob_ind = mmap(NULL, sizeof *glob_ind, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);

    int checkout = 0;
    int Md5check = 2;

    int pid = 0;
    int fork_flag = 0;
    int ppid = 0;

    struct timespec delay;
    delay.tv_nsec = 0;
    delay.tv_sec = 1;

    for (i = 0; i < workers; i++)                                                                      // creating workers
    {
        pid = fork();                                                                                  // creating child process
        fork_flag = 0;
        switch(pid)
        {
            case -1:
                fprintf(stderr,"Process %i creating error", i);
                break;
            case 0:
                if ( i == 0 )
                    *glob_var = getpid();                                                               // saving pid of first process to changing process group off all childrens
                fork_flag = 1;
                break;
            default:
                break;
        }
        if (fork_flag == 1)
            break;
    }

    if (pid == 0)
    {
        prctl(PR_SET_PDEATHSIG, SIGHUP);                                                                // request to SIGHUP for children when parent die
        ppid = getppid();
        pid_t pid_worker = getpid();
        if (setpgid(pid_worker, *glob_var) != 0)                                                        // changing process group of all children processes
            fprintf(stderr,"child setpgid errno %s\n", strerror(errno));

        while(*glob_ind < len)                                                                          // loop till all bytes will be send
        {
            do_sleep(clock_time);                                                                       // waiting for SIGALRM
            if(read(fd[0], &readbuffer, 1) <=0)                                                         // reading from parent process
            {
                printf("read() failed: [%d][%s]\n", errno, strerror(errno));
                exit(1);
            }
            package.byte = readbuffer;
            clock_gettime(CLOCK_REALTIME, &package.real_time);                                          // added time stemple to send package
            printf("Process [%d]: Sending byte %c\n", pid_worker, readbuffer);
            sendto(w_fd[i], &package, size_p, 0, (struct sockaddr *) &w_addr[i], addr_size);           // sending package to archiver
            (*glob_ind)++;                                                                             // incrementing global shared variable

        }
        send_signal(ppid);                                                                             // send SIGALRM to parent that work has been done
        for ( i = 0; i < workers; i++ )
        {
            free(w_names[i]);
            close(w_fd[i]);
        }
        free(w_names);
        free(message);
        free(address_p);
        close(fd[0]);
        close(fd[1]);
        exit(0);
    }
    else                                                                                               // parent block
    {
        signal(SIGALRM, &sig_handler);                                                                 // connecting handler with signal receiving that childrens done
        signal(SIGUSR1, &sig_handler);
        write(fd[1], message, len);                                                                    // write message from -m parameter to child processes
        while(g_running)                                                                               // waiting till child processes
        {
            nanosleep(&delay, NULL);
            if(md5_check == 1)
                write(fd_P,&Md5check,sizeof(int));
        }
        checkout = 1;
        write(fd_P,&checkout,sizeof(int));                                                              // send flag to archiwer that workers done with sending
        if(md5_check == 1)                                                                              // if received SIGUSR1 rewuest to Archiwist to provide md5 checksum
        {
            read(fd_P,md5,sizeof(md5));
            printf("md5 new %s\n",md5);
            int cmp = -1;
            if((cmp = strcmp(md5,id.md5)) == 0);
            printf("Archiwist confirmed that he successfully received message\n");
        }
        for ( i = 0; i < workers; i++ )                                                                // closing sockets and free memory
        {
            free(w_names[i]);
            close(w_fd[i]);
        }
        free(w_names);
        free(message);
        close(fd[0]);
        close(pub_soc);
        munmap(glob_var, sizeof *glob_var);
        munmap(glob_ind, sizeof *glob_ind);
        close(fd[1]);
    }

    return 0;
}

