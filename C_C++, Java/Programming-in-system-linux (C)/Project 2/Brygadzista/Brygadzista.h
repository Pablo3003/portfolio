#ifndef BRYGADZISTA_BRYGADZISTA_H
#define BRYGADZISTA_BRYGADZISTA_H
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <openssl/md5.h>
#include <time.h>
#include <sys/prctl.h>

#define MAX 50
#define MAX_ID 100
#define ACTUAL_ID 5


extern volatile int g_running;
extern volatile int md5_check;
extern int *glob_var;
extern int *glob_ind;


int public_connection(char* address);
int private_connection(char* name,char** names, int workers,  int* w_fd, struct sockaddr_un* w_cont);
char* randstring(size_t length);
void md5sum(char* str, char* out, int size);
void handle_sigalrm(int signal);
void do_sleep(int seconds);
void sig_handler(int signo);
void send_signal (int pid);


struct Identifier                                               //  send to identify task
{
    int workers;
    char name[MAX_ID];
    ssize_t msg_len;
    pid_t boss_pid;
    char md5[33];
};

struct Message                                                  // send by workers to archiver
{
    char byte;
    struct timespec real_time;
};

#endif //BRYGADZISTA_BRYGADZISTA_H
