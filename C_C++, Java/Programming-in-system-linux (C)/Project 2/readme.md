Polecenie do zadania


1. Brygada i Archiwista - wersja podstawowa

Brygada składa się z Brygadzisty i ustalonej liczby Robotników. Brygadzista ma za zadanie

wysłać komunikat do Archiwisty i korzysta w tym celu z pracy Robotników. Każdy z Robotników,

we wskazanych momentach czasu, pobiera fragment komunikatu i przesyła go bezpośrednio do

Archiwisty. Zadaniem Archiwisty jest złożenie wszystkich fragmentów w całość.

1.1. Architektura brygady

— Brygada wymaga istnienia Brygadzisty. Jego śmierć musi pociągać za sobą rozwiązanie bry-
gady.

— Brygada składa się z ustalonej liczby Robotników.

— Tworzeniem Robotników i pilnowaniem, by była ich odpowiednia liczba, zajmuje się Bry-
gadzista.

— Wszystkie Robotniki w brygadzie należą do jednej grupy procesów.

Ma to być grupa bez lidera, w której, poza Robotnikami, nie ma innych procesów.

— Brygada posiada identyfikator. Jest on podawany w parametrze przy tworzeniu brygady.

— Identyfikator identyfikuje zadanie, a nie procesy brygady.

— Celem jego stosowania jest realizacja scenariusza, w którym działalność brygady zostanie

wznowiona (nowe procesy) po jej rozwiązaniu.

Identyfikator ma umożliwić powiązanie działań nowej brygady z poprzednimi wynikami.

— Brygadzista komunikuje się z Robotnikami przez wspólny kanał komunikacyjny – pipe. (Bry-
gadzista zapisuje, Robotniki odczytują).

1.2. Komunikacja Brygady z Archiwistą

Zadaniem Archiwisty jest gromadzenie wiadomości od brygad. Brygada komunikuje się z Ar-
chiwistą za pomocą prywatnych kanałów, realizowanych za pomocą interfejsu gniazd.

Dla każdego procesu w brygadzie przypada jeden prywatny kanał. Aby uzyskać prywatne

kanały, brygada musi się u Archiwisty zarejestrować. Odpowiedzialny jest za to Brygadzista.

Archiwista może obsługiwać jednocześnie wiele brygad, które rozróżnia po kanałach, z których

korzystają.

Na potrzeby rejestracji Archiwista udostępnia jeden ogólnie dostępny kanał. Ponieważ jest

to kanał publiczny, rejestrowana brygada musi być zidentyfikowana przez podanie identyfikatora

brygady (zadania).

W czasie rejestracji tworzony jest pierwszy prywatny kanał, przeznaczony dla Brygadzisty.

Kolejne prywatne kanały dla tej brygady, są tworzone z wykorzystaniem tego pierwszego.

Każdy z Robotników posiada własny prywatny kanał. Jeżeli Robotnik zginie, a w jego miejsce

powstanie nowy, to dziedziczy on kanał poprzednika. Jeżeli zginie Brygadzista, to Archiwista

powinien usunąć wszystkie kanały należące do jego brygady.

Po zakończeniu wysyłania Brygadzista zgłasza Archiwiście wyrejestrowanie zadania. Możli-
we jest też odpytanie Archiwisty o sumę kontrolną oczytanej przez niego wiadomości. W obu

przypadkach używany jest prywatny kanał Brygadzisty.

1.3. Wysyłanie komunikatów

Wiadomość do wysłania jest umieszczona przez Brygadzistę we wspólnym kanale komunika-
cyjnym brygady. Następnie, w odpowiednich momentach czasu, Brygadzista informuje Robotni-
ków, że należy wysłać kolejne fragmenty do Archiwisty. Informacja ta jest przekazana za pomocą

jednego sygnału SIGALRM, wysyłanego do wszystkich Robotników na raz.

1

Po otrzymaniu sygnału, każdy Robotnik odczytuje jeden bajt komunikatu i wysyła go do Archi-
wisty, używając do tego swojego prywatnego kanału.

Należy przyjąć, że komunikacja między Robotnikami a Archiwistą jest potencjalnie zawodna

(fragmenty komunikatów mogą pojawiać się w złej kolejności, być powielane lub być gubio-
ne). Dlatego, każdy wysyłany komunikat, jest przez Robotnika opatrywany stemplem czasowym.

Stemple te są jedynym narzędziem wspomagającym Archiwistę w odtwarzaniu całej wiadomości.

1.4. Szczegóły techniczne

Kanały komunikacji Brygada–Archiwista są w domenie UNIX. Wszystkie prywatne kanały

korzystają z „abstrakcyjnej przestrzeni nazw” (rozszerzenie Linux). Typy poszczególnych gniazd:

— publiczne – strumień,

— prywatne Brygadzisty – strumień,

— prywatne Robotników – datagramy.

Jako sumy kontrolnej należy użyć MD5 lub SHA-1.

Parametry uruchamiania mają opisywać następujące właściwości (przynajmniej):

— Archiwista

— adres kanału do rejestracji,

— Brygada

— adres kanału do rejestracji,

— ilość robotników,

— komunikat,

— momenty wysyłania fragmentów komunikatu.

1.5. Uwagi

Rozwiązując zadanie należy samodzielnie dopracować szczegóły, np.:

— protokóły (rejestracja, tworzenie gniazd prywatnych, . . . ),

— użycie stempli do odtwarzania wiadomości (w tym dobranie momentu pobierania stempla

tak, by spełniał on swoje zadanie),

— parametry uruchamiania (wiadomość do wysłania, . . . ).
