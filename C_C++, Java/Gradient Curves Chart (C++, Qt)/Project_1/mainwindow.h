#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QApplication>
#include <QMainWindow>
#include <QtGui>
#include <QtCore>
#include <QDebug>
#include <QSize>
#include <QtWidgets>
#include <QAction>
#include <QFileDialog>
#include <QMessageBox>
#include <QClipboard>
#include "plot.h"
#include "resource.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void open();
    void copy();
    void next();
    void previous();

private:
    Plot* plot;
    quint16 flag;

    void createActions();
    Ui::MainWindow *ui;
    void paintEvent(QPaintEvent *e);
};

#endif // MAINWINDOW_H
