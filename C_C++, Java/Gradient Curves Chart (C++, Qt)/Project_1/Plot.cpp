#include "plot.h"


Plot::Plot(QSize size)
{
     xstart = size.width()/10;
     ystart = size.height()/7;
     xend = size.width()/10*7;
     yend = size.height()/7*6;

     srand(time(NULL));
     for(int i=0; i<38; i++)
     {
         int r = rand() % 255;
         int g = rand() % 255;
         int b = rand() % 255;
         QColor temp(r,g,b);
         color[i] = temp;
     }
     start_set = 0;
     end_set = 38;
     constants_set = 0;
}

Plot::Plot(){}

void Plot::NextSet()
{
    start_set+=38;
    end_set+=38;
    constants_set+=114;
}

void Plot::PreviousSet()
{
    start_set-=38;
    end_set-=38;
    constants_set-=114;
}

void Plot::setRes(Resource* resource)
{
    vect_x = resource->GetX();
    vect_y = resource->GetY();
    constants = resource->GetConstats();
}

void Plot::MainGrid(QPainter *painter, QSize size)
{
    xstart = size.width()/10;
    ystart = 0;
    xend = size.width()/10;
    yend = 0;

    QPointF point_text;
    QString axis = "10";
    QString axisX = "L/d";
    QString axisY = "Ra/Rm";
    QString header = "Rt/Rm";
    QString horizontal_axis2[6] = {"4","3","2","1","0","-1"};

    for(int i=0; i<6; i++)
    {
        painter->drawLine(xstart,ystart+=size.height()/7,xend*7,yend+=size.height()/7);
        point_text.setX(xstart - size.width()/25);
        point_text.setY(size.height()/7*(i+1));
        painter->drawText(point_text,axis);
        point_text.setX(xstart - size.width()/25+20);
        point_text.setY(size.height()/7*(i+1)-10);
        painter->drawText(point_text,horizontal_axis2[i]);
    }

    ystart = size.height()/7;
    yend = size.height()/7;

    for(int i=0; i<4; i++)
    {
        painter->drawLine(xstart,ystart,xend,yend*6);
        point_text.setX(xstart-10);
        point_text.setY(yend*6+size.height()/25);
        painter->drawText(point_text,axis);
        point_text.setX(xstart+10);
        point_text.setY(yend*6+size.height()/25-10);
        painter->drawText(point_text,horizontal_axis2[5-i]);
        xstart+=size.width()/5;
        xend+=size.width()/5;
    }
    point_text.setX(2*size.width()/5);
    point_text.setY(yend*6+size.height()/15);
    painter->drawText(point_text,axisX);
    point_text.setX(size.width()/30);
    point_text.setY(yend*3.5);
    painter->drawText(point_text,axisY);
    point_text.setX(size.width()/10*8);
    point_text.setY(ystart - size.height()/40);
    painter->drawText(point_text,header);


}

void Plot::LogGrid(QPainter *painter,QSize size)
{
    xstart = size.width()/10;
    xend = size.width()/10;
    float logwidth = size.width()/11.5;
    ystart = size.height()/7;
    yend = size.height()/7;
    float logheight = size.height()/16;

    for(int j=0; j<5; j++)
    {
        for(int i=2; i<10; i++)
        {
            painter->drawLine(xstart,log(i)*-logheight+ystart*(6-j),xend*7,log(i)*-logheight+yend*(6-j));
            if(j<3)
                painter->drawLine(log(i)*logwidth+xstart*(j*2+1),ystart,log(i)*logwidth+xend*(j*2+1),yend*6);
        }
    }
}

void Plot::Chart(QPainter *painter,QSize size)
{
    QPointF zip[119];
    QPointF point_text;
    QString text = "D/d = " + constants[constants_set]+ "    Ri/Rm = " + constants[constants_set+1];
    xstart = size.width()/10;
    ystart = size.height()/7;
    xend = size.width()/10*7;
    yend = size.height()/7*6;
    quint16 counterX = 0;
    QPen pen(1);
    pen.setWidth( 3 );

    for(int j=start_set; j<end_set; j++)
    {
        pen.setColor(color[counterX]);
        painter->setPen(pen);
        for(int i = 0; i < 119; i++)
        {
            zip[i].setX(vect_x[i]*size.width()/5+xstart+size.width()/5);
            zip[i].setY(vect_y[j][i]*-size.height()/7+size.height()/7*5);

        }
        painter->drawPolyline(zip,119);
        counterX++;
    }
    pen.setColor(Qt::black);
    painter->setPen(pen);
    point_text.setX(size.width()/7);
    point_text.setY(size.height()/8);
    painter->drawText(point_text,text);
}

void Plot::Legend(QPainter *painter, QSize size)
{
    QPointF point[2];
    QPointF point_text;
    QPointF text_offset(size.width()/30,0);
    QPen pen(1);
    pen.setWidth( 10 );
    QFont font = painter->font();
    font.setPixelSize(15);

    point[0].setY(size.height()/7);
    point[1].setY(size.height()/7);
    quint16 counter = 0;
    quint16 const_num = constants_set+2;

    for(int j =1; j<20; j++)
    {
        point[0].setX(size.width()/10*7.5);
        point[1].setX(size.width()/10*7.5+size.width()/100);
        for(int i=1; i<3; i++)
        {
            pen.setColor(color[counter]);
            counter++;
            painter->setPen(pen);
            painter->setFont(font);
            painter->drawPolyline(point,2);
            point_text = point[0].operator +=(text_offset);
            painter->drawText(point[0], constants[const_num]);
            const_num +=3;
            point[0].setX(size.width()/10*9);
            point[1].setX(size.width()/10*9+size.width()/100);

        }
        point[0].setY(point[0].y()+ size.height()/25);
        point[1].setY(point[1].y()+ size.height()/25);
    }
}


