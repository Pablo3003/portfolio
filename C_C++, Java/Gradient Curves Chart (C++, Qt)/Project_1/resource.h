#ifndef RESOURCE_H
#define RESOURCE_H
#include <QString>
#include <QVector>
#include <iostream>
#include <fstream>
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <QArrayData>
#include <QSize>


class Resource
{
public:
    Resource(qint32 count=0);
    ~Resource();
    void Readbin(QString filename);
    QVector <float> GetX(){return vect_x;}
    QVector < QVector <float>> GetY(){return vect_y;}
    QVector <QString> GetConstats(){return constants;}

private:
    quint32 counterSet;
    QVector <float> vect_x;
    QVector < QVector <float>> vect_y;
    QVector <QString> constants;
};
#endif // RESOURCE_H
