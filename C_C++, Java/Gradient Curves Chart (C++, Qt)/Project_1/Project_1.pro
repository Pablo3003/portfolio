#-------------------------------------------------
#
# Project created by QtCreator 2017-03-16T08:24:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Project_1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    resource.cpp \
    plot.cpp

HEADERS  += mainwindow.h \
    resource.h \
    plot.h

FORMS    += mainwindow.ui
