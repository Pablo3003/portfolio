#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), plot(new Plot(this->size()))
{
    ui = new Ui::MainWindow;
    ui->setupUi(this);
    this->setWindowTitle("Charts");

    flag = 0;
    createActions();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::open()
{
    Resource* res = new Resource();
    QString filename = QFileDialog::getOpenFileName(this,tr("Open File"),"../Resources","All files(*.*);;Data files(*.dat");
    res -> Readbin(filename);
    plot->setRes(res);
    QMessageBox::information(this,tr("File Name"),filename);
    ui->actionNext->setEnabled(true);

    flag = 1;
}

void MainWindow::copy()
{
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setPixmap(QPixmap::grabWidget(this));
}

void MainWindow::next()
{
    ui->actionPrevious->setEnabled(true);
    plot->NextSet();
    MainWindow::repaint();
    if(plot->getConstantsSet() > 17100)
        ui->actionNext->setDisabled(false);
}

void MainWindow::previous()
{
    ui->actionNext->setEnabled(true);
    plot->PreviousSet();
    MainWindow::repaint();
    if(plot->getConstantsSet() < 114 )
        ui->actionPrevious->setDisabled(true);
}

void MainWindow::createActions()
{
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::open);
    connect(ui->actionCopy, &QAction::triggered, this, &MainWindow::copy);
    connect(ui->actionNext, &QAction::triggered, this, &MainWindow::next);
    connect(ui->actionPrevious, &QAction::triggered, this, &MainWindow::previous);
}

void MainWindow::paintEvent(QPaintEvent *e)
{
    QSize size = this->size();
    QPainter painter(this);
    plot -> MainGrid(&painter, size);
    QPen pen(Qt::gray);
    painter.setPen(pen);
    plot -> LogGrid(&painter, size);
    if(flag == 1)
    {
        plot -> Chart(&painter, size);
        plot -> Legend(&painter, size);
    }
}

