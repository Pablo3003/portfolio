#ifndef PAINT_H
#define PAINT_H
#include <QMainWindow>
#include <QtGui>
#include <QtCore>
#include <QApplication>
#include <QPainter>
#include <QLineF>
#include <QPoint>
#include <QTransform>
#include <math.h>
#include <time.h>
#include "resource.h"


class Plot
{
  public:
    Plot(QSize size);
    Plot();
    ~Plot();
    void MainGrid(QPainter *painter, QSize size);
    void LogGrid(QPainter *painter, QSize size);
    void Chart(QPainter *painter, QSize size);
    void Legend(QPainter *painter, QSize size);
    void NextSet();
    void PreviousSet();
    quint32 getConstantsSet(){return constants_set;}
    void setRes(Resource* resource);

private:
    QVector <float> vect_x;
    QVector < QVector <float>> vect_y;
    QVector <QString> constants;
    QColor color[38];
    quint32 start_set;
    quint32 end_set;
    quint32 constants_set;
    float xstart;
    float xend;
    float ystart;
    float yend;
};

#endif // PAINT_H
