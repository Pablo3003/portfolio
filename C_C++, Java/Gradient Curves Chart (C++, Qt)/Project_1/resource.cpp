#include "resource.h"

Resource::Resource(qint32 count)
{
    counterSet = count;
}


void Resource::Readbin(QString filename)
{
    QFile file(filename);

    const float size = file.size()/sizeof(float);
    QVector<float> vect(size);

    if (file.open(QIODevice::ReadOnly))
    {
        file.read(reinterpret_cast<char*>(&vect[0]), size);
    }

    for (int i=0; i<120; i++)               // first 120 numbers are X coordinates
    {
        vect_x.append(vect[i]);
    }

    qint16 counterConstants = 0;
    quint16 j = 1;
    quint16 n = 0;
    for (int i=120; i<size-120; i++)
    {
       constants.append(QString::number(vect[i]));
       counterConstants++;
       if((counterConstants % 3) == 0 && (counterConstants > 2))
       {
          n++;
          vect_y.append(QVector <float>());
          for (j=1; j<120; j++)
          {
             vect_y[counterSet].append(vect[i+j]);
          }
          i += j;
          counterSet++;
       }
    }
}
