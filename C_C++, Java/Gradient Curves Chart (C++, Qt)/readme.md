

***************************************************************************
Application to visualization gradient and potential curves basing on data readed from binary files in specific format.

Requirements:
- Data reading from binary file where was wrote coordinates sequence: 120 X, 3 constants, 120 Y, 3 constants, 120 Y, 3 constants...
- Responsive chart
- 38 charts on one view, next and previous buttons to navigate beetween chart sets
- Copy button to copy chart to clipboard
- Logarythmic scale


                                
        begin                : 2017-03-16
        copyright            : (C) 2016 by Paweł Marszalec
        email                : pawel.marszalec92@gmail.com
***************************************************************************

![picture](img/img.PNG)



