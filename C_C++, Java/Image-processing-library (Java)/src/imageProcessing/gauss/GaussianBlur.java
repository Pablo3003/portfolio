package imageProcessing.gauss;

import imageProcessing.ImageFile;
import imageProcessing.Pixel;


public class GaussianBlur {


    // shorten version to get red value of pixel
    private static double getR(int x,int y, ImageFile image){ return image.getPixel(x,y).getRed(); }

    // shorten version to get green value of pixel
    private static double getG(int x,int y, ImageFile image){
        return image.getPixel(x,y).getGreen();
    }

    // shorten version to get blue value of pixel
    private static double getB(int x,int y, ImageFile image){
        return image.getPixel(x,y).getBlue();
    }


    /**
     * get matrix of new colors depending from radius (mask size).
     * @param x The blur radius + x coordinate of pixel
     * @param y The blur radius + y coordinate of pixel
     * @param whichColor The 1 - red, 2 - green, 3 - blue
     * @param blurRadius parameter defining blur radius of Gaussian blur
     * @param image original image
     * @return double[][] array contains new colors
     */

    private static  double[][] getColorMatrix(int x, int y, int whichColor, int blurRadius, ImageFile image){

        int startX = x-blurRadius;          // as given x = original x + blurRadius
        int startY = y-blurRadius;          // as given y = original y + blurRadius
        int counter = 0;

        int length = blurRadius*2+1;        // size of mask defined by radius
        double[][] arr = new double[length][length];

        // mask around the pixel defined by radius
        for (int i=startX ; i<startX+length ;i++){                              // each x of fragment
            for (int j = startY; j < startY+length; j++){                       // each y of fragment
                if (whichColor == 1){
                    arr[counter%length][counter/length] = getR(i, j, image);    // full-filling array if color is red
                }else if (whichColor == 2){
                    arr[counter%length][counter/length] = getG(i, j, image);    // full-filling array if color is green
                }else if (whichColor == 3){
                    arr[counter%length][counter/length] = getB(i, j, image);    // full-filling array if color is blue
                }
                counter++;
            }
        }

        return arr;
    }

    /**
     * get weight matrix depending from radius .
     * @param blurRadius parameter defining blur radius of Gaussian blur
     * @param weightArr original weightArray
     */

    private static void calculateWeightMatrix(int blurRadius, double[][] weightArr){

        for (int i=0;i<blurRadius*2+1;i++){             // for each x in fragment of image defined by blur radius
            for (int j=0;j<blurRadius*2+1;j++){         // for each y in fragment of image defined by blur radius
                weightArr[i][j] = getWeight(j-blurRadius,blurRadius-i, blurRadius); // creating weight array of fragment using getWeight function
            }
        }

    }

    /**
     * get weight of pixel depending from radius .
     * @param blurRadius parameter defining blur radius of Gaussian blur
     * @param x The x coordinate of pixel
     * @param y The y coordinate of pixel
     */

    private static double getWeight(int x,int y, int blurRadius){

        double sigma = (blurRadius*2+1)/2;              // sigma for Gauss calculation
        double weight = (1/(2*Math.PI*sigma*sigma))*Math.pow(Math.E,((-(x*x+y*y))/((2*sigma)*(2*sigma)))); // gauss

        return weight;  // calculated weight
    }

    /**
     * get final weight matrix depending from radius and weight array .
     * @param blurRadius parameter defining blur radius of Gaussian blur
     * @param weightArr original weightArray
     */

    private static void getFinalWeightMatrix(int blurRadius, double[][] weightArr){

        int length = blurRadius*2+1;        //diameter of fragment
        double weightSum = 0;

        for (int i = 0;i<length;i++){       // each x in fragment
            for (int j=0; j<length; j++ ){  // each y in fragment
                weightSum+=weightArr[i][j];     // sum of weights
            }
        }


        for (int i = 0;i<length;i++){                       // each x in fragment
            for (int j=0; j<length; j++ ){                      // each y in fragment
                weightArr[i][j] = weightArr[i][j]/weightSum;    // final weight is weight divided by sum of weights
            }
        }
    }

    /**
     * get new color of pixel depending from radius and weight array .
     * @param x The x coordinate of pixel
     * @param y The y coordinate of pixel
     * @param blurRadius parameter defining blur radius of Gaussian blur
     * @param weightArr original weightArray
     * @param image original image
     */

    private static double getBlurColor(int x, int y,int whichColor, int blurRadius, double[][] weightArr, ImageFile image){

        double blurGray = 0;
        double[][] colorMat = getColorMatrix(x,y,whichColor, blurRadius, image);

        int length = blurRadius*2+1;
        for (int i = 0;i<length;i++){
            for (int j=0; j<length; j++ ){
                blurGray += weightArr[i][j]*colorMat[i][j];
            }
        }

        return blurGray;
    }


    /**
     * get new image after gaussian blur depending from blur radius .
     * @param blurRadius parameter defining blur radius of Gaussian blur
     * @param image original image
     */

    public static ImageFile getBluredImg(ImageFile image, int blurRadius){

        double[][] weightArr = new double[blurRadius*2+1][blurRadius*2+1];          // new empty array
        calculateWeightMatrix(blurRadius, weightArr);                           // weight matrix
        getFinalWeightMatrix(blurRadius, weightArr);                            // final weight matrix where weight = weight/sum of weights

        ImageFile bi = new ImageFile(image.getWidth()-blurRadius*2, image.getHeight()-blurRadius*2);
        for (int x = 0; x < bi.getWidth(); x++) {       // each x of image
            for (int y = 0; y < bi.getHeight(); y++) {  // each y of image
                int r = (int)getBlurColor(blurRadius+x,blurRadius+y,1, blurRadius, weightArr, image);   // get new color for red channel
                int g = (int)getBlurColor(blurRadius+x,blurRadius+y,2, blurRadius, weightArr, image);   // get new color for green channel
                int b = (int)getBlurColor(blurRadius+x,blurRadius+y,3, blurRadius, weightArr, image);   // get new color for blue channel
                Pixel pixel = new Pixel(r,g,b,255);     // for pixel setting purpose
                bi.getPixel(x, y).setPixel(pixel);          // setting new pixels in image
            }
        }
        return bi;
    }

}