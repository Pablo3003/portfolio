package imageProcessing;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Kirsch implements ImageProcessing {
	private static final int MAX_COLOR = 255;
	private ImageFile input; //input file
	private ImageFile output; //output file 
	private ImageFile modified; 
	// eight Kirsch masks rotated to each other by 45 degrees
	public static int[][][] maskKirsh = 
			{ { { -3, -3, 5 }, { -3, 0, 5 }, { -3, -3, 5 } },
			{ { -3, 5, 5 }, { -3, 0, 5 }, { -3, -3, -3 } }, 
			{ { 5, 5, 5 }, { -3, 0, -3 }, { -3, -3, -3 } },
			{ { 5, 5, -3 }, { 5, 0, -3 }, { -3, -3, -3 } },
			{ { 5, -3, -3 }, { 5, 0, -3 }, { 5, -3, -3 } },
			{ { -3, -3, -3 }, { 5, 0, -3 }, { 5, 5, -3 } },
			{ { -3, -3, -3 }, { -3, 0, -3 }, { 5, 5, 5 } },
			{ { -3, -3, -3 }, { -3, 0, 5 }, { -3, 5, 5 } }, };

    /**
     * Create new object and allocate memory for input, and read an image
     * @param path path of the file to load
     */
	public Kirsch(String path) throws IOException {
		input = new ImageFile(); // allocating memory for input image
		loadData(path);

	}
    /**
     * Load an image.
     * @param path path of the file to load
     */
	@Override
	public void loadData(String path) throws IOException {
		input.loadData(path);
		output = new ImageFile(input.getWidth(), input.getHeight()); //output for grayscale has same size as input
	}
    /**
     * Starts applying Kirsch mask proccess by crating a grayscale image.
     */
	public void KirschMask() {
		averageGrayscaleAndKirsch(input); //apply grayscale on image 
	}

	@Override
    /**
     * Save the result
     * @param path path of the file to save
     * @param type determines if result should be normalized and saved as an image or as a text
     */
	public void saveData(String path, Integer type) throws IOException {
		modified.saveData(path, input.getType());    //save modified image
	}

	@Override
    /**
     * Get image type
     * @return integer code of image type
     */
	public int getType() {
		return input.getType();

	}
    /**
     * Apply mask on image
     * @param image array which represents image values
     * @param mask array which represents mask values
     * @return integer array of computed values
     */
	private static int[][] applyMask(int[][] image, int[][] mask) {
		int height = image.length; //get image height value
		int width = image[0].length; //get image width value
		int mid = (int) Math.floor(mask.length / 2); // in our case 1
		int[][] result = new int[height][width]; //array with result values
		int total = 0;
		for (int i = 0; i < mask.length; i++) {
			for (int j = 0; j < mask[0].length; j++) {
				total += mask[i][j];      //addding next mask in order
			}
		}

		for (int i = mid; i < height - mid; i++) {
			for (int j = mid; j < width - mid; j++) {
				int value = 0;
				for (int mi = -mid; mi <= mid; mi++) {
					for (int mj = -mid; mj <= mid; mj++) {
						value = value + (image[i + mi][j + mj] * mask[mid + mi][mid + mj]); // getting new values with mask applied
					}
				}
				result[i][j] = Math.abs(value); //result is absolute of counted value
			}
		}
		return result;
	}
    /**
     * Create  grayscale image 
     * @param original The image on which operation is performed
     */
	public void averageGrayscaleAndKirsch(ImageFile original) {
		int alpha, red, green, blue;
		int newPixel;
		int[] avgLUT = new int[766]; //array for all colors(red+green+blue)
		for (int i = 0; i < avgLUT.length; i++) {
			avgLUT[i] = (int) (i / 3); //new value equals (red + green + blue)/3 
		}

		for (int i = 0; i < original.getWidth(); i++) {
			for (int j = 0; j < original.getHeight(); j++) {
				alpha = original.getPixel(i, j).getAlpha(); //get alpha
				red = original.getPixel(i, j).getRed(); //get red 
				green = original.getPixel(i, j).getGreen(); //get green 
				blue = original.getPixel(i, j).getBlue(); //get blue 
				newPixel = red + green + blue;  
				newPixel = avgLUT[newPixel]; //new value of pixel 
				
				output.getPixel(i, j).setAlpha(alpha);
				output.getPixel(i, j).setRed(newPixel); // Set new red value 
				output.getPixel(i, j).setGreen(newPixel); // Set new green value 
				output.getPixel(i, j).setBlue(newPixel); // Set new blue value 
			}
		}
		int[][][] modifiedIntensties = new int[maskKirsh.length][output.getHeight()][output.getWidth()]; //array for levels of modified intensities
		int[][] intesities = new int[output.getWidth()][output.getHeight()]; //array for levels of intensities
		for (int i = 0; i < output.getWidth(); i++) {
			for (int j = 0; j < output.getHeight(); j++) {
				intesities[i][j] = output.getPixel(i, j).getRed(); // all colors have the same value
			}
		}

		for (int i = 0; i < maskKirsh.length; i++) {
			modifiedIntensties[i] = applyMask(intesities, maskKirsh[i]); // 8 masks applying to intensities
		}
		modified = new ImageFile(output.getWidth(), output.getHeight()); //new ImageFile for output
		for (int i = 0; i < modified.getWidth(); i++) {
			for (int j = 0; j < modified.getHeight(); j++) {
				int max = modifiedIntensties[0][i][j];
				for (int x = 1; x < modifiedIntensties.length; x++) {
					if (modifiedIntensties[x][i][j] > max) {
						max = modifiedIntensties[x][i][j]; //set maximum value from all masks
					}
				}
				if (max > MAX_COLOR) //in case max > 255
					max = MAX_COLOR;
				/* set new values to pixels */
				modified.getPixel(i, j).setAlpha(output.getPixel(i, j).getAlpha());
				modified.getPixel(i, j).setRed(max);
				modified.getPixel(i, j).setGreen(max);
				modified.getPixel(i, j).setBlue(max);

			}
		}
	}

}
