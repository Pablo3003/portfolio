package imageProcessing;

import java.io.IOException;


public class ImageResize implements ImageProcessing{
    private ImageFile input;        // input file
    private ImageFile output;       // output file

    /**
     * Create ImageResize object and allocate memory for input, and read an image
     * @param inputPath path of the file to load
     */
    public ImageResize(String inputPath) throws IOException {
        input = new ImageFile();    // allocate memory for input image
        loadData(inputPath);        // load input image from path
    }

    /**
     * Create ImageResize object and allocate memory for input
     */
    ImageResize() {
        input = new ImageFile();
    }   // create allocate memory

    @Override
    /**
     * Load an image.
     * @param path path of the file to load
     */
    public void loadData(String path) throws IOException {
        input.loadData(path);   // read image from path
    }

    /**
     * Resize the image .
     * @param ratio ratio of the scaling
     */
    public void resize(float ratio)
    {
        output = new ImageFile((int)(input.getWidth()*ratio), (int)(input.getHeight()*ratio));  // new image with size multiplied by ratio

        for (int y = 0; y < output.getHeight(); ++y)    // for loop to access every row of image
        {
            float v = (float)(y) / (float)(output.getHeight() - 1); // calculate height value to interpolate
            for (int x = 0; x < output.getWidth(); ++x)     // for loop to access every column
            {
                float u = (float)x / (float)(output.getWidth() - 1); // calculate with value to interpolate

                Pixel pixel = bicubicInterpolation(u,v);    // interpolation
                output.getPixel(x,y).setPixel(pixel);       // assign interpolated pixel to output image
            }
        }
    }

    private Pixel bicubicInterpolation(float u, float v)
    {
        Pixel p[][] = new Pixel[4][4];
        for (int i=0; i<4; i++)
            for (int j=0; j<4; j++)
                p[i][j]=new Pixel();    // prepare array for 16 pixels (bicubic)

        int q,w;

        float x = (u * input.getWidth()) - 0.5f;    // x to interpolate
        int xint = (int)x;
        float dx = x - (int)x;                      // distance on x

        float y = (v * input.getHeight()) - 0.5f;   // y to interpolate
        int yint = (int)y;
        float dy = y - (int)y;                      // distance on y

        for(int i=0; i<4; i++)
        {
            for(int j=0; j<4; j++)
            {
                q=xint - 1 + j;                     // which pixel we need to take
                q=ImageFile.bounds(q,0, input.getWidth()-1);    // keep it in bounds of an original image
                w=yint - 1 + i;
                w=ImageFile.bounds(w,0, input.getHeight()-1);   // same to y pixels
                p[j][i] = input.getPixel(q,w);
            }
        }
        Pixel el1 = interpolation(p[0][0],p[1][0], p[2][0], p[3][0], dx);
        Pixel el2 = interpolation(p[0][1],p[1][1], p[2][1], p[3][1], dx);
        Pixel el3 = interpolation(p[0][2],p[1][2], p[2][2], p[3][2], dx);
        Pixel el4 = interpolation(p[0][3],p[1][3], p[2][3], p[3][3], dx);   // interpolate for every line gives four points ( in one axis )
        Pixel value = interpolation(el1, el2, el3, el4, dy);                // orthogonal interpolation (second axis)

        return value;
    }

    private Pixel interpolation(Pixel A, Pixel B, Pixel C, Pixel D, float t)
    {
        float[] a = new float[3];       // creating some arrays and assigning values
        float[] b = new float[3];
        float[] c = new float[3];
        float[] d = new float[3];
        Pixel toRet = new Pixel();
        a[0]=A.getRed();
        b[0]=B.getRed();
        c[0]=C.getRed();
        d[0]=D.getRed();

        a[1]=A.getGreen();
        b[1]=B.getGreen();
        c[1]=C.getGreen();
        d[1]=D.getGreen();

        a[2]=A.getBlue();
        b[2]=B.getBlue();
        c[2]=C.getBlue();
        d[2]=D.getBlue();

        float[] w = new float[3];
        float[] x = new float[3];
        float[] y = new float[3];
        float[] z = new float[3];

        float[] color = new float[3];

        for(int i=0; i<3; i++)  // for every color
        {
            w[i]= -a[i] / 2.0f + (3.0f*b[i]) / 2.0f - (3.0f*c[i]) / 2.0f + d[i] / 2.0f; // hermite interpolation
            x[i]= a[i] - (5.0f*b[i]) / 2.0f + 2.0f*c[i] - d[i] / 2.0f;
            y[i]= -a[i] / 2.0f + c[i] / 2.0f;
            z[i]= b[i];
            color[i]= w[i] * t*t*t + x[i] * t*t + y[i] * t +z[i];
            color[i]=ImageFile.bounds(color[i],0,255);                      // and we get color in range 0-255
        }
        toRet.setRed((int)color[0]);    // set values of pixel to return
        toRet.setGreen((int)color[1]);
        toRet.setBlue((int)color[2]);
        toRet.setAlpha(255);

        return toRet;
    }

    @Override
    /**
     * Get image type
     * @return integer code of image type
     */
    public int getType() { return input.getType(); }    // get type of image (png, gif, etc.)

    @Override
    /**
     * Save the result
     * @param path path of the file to save
     * @param type type of the image to save
     */
    public void saveData(String path, Integer type) throws IOException{
        output.saveData(path, type);    // save our resized image
    }
}
