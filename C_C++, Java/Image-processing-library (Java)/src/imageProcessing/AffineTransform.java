package imageProcessing;
import java.io.IOException;

public class AffineTransform implements ImageProcessing {
	private ImageFile input;
	private ImageFile outputAfterScaling;
	private ImageFile outputAfterTranslate;
	private ImageFile outputAfterRotation;
	
    /**
     * Create new object and allocate memory for input, and read an image
     * @param path path of the file to load
     */
	public AffineTransform(String path) throws IOException {
		input = new ImageFile(); 
		loadData(path); //loading input image
	}
    /**
     * Load an image, create output images
     * @param path path of the file to load
     */
	public void loadData(String path) throws IOException {
		input.loadData(path);
		int in_w = input.getWidth();
		int in_h = input.getHeight();
		outputAfterScaling = new ImageFile(in_w, in_h); //output image for scaling
		outputAfterTranslate = new ImageFile(in_w, in_h); //output image for translating
		outputAfterRotation = new ImageFile(in_w, in_h); //output image for rotating
	}
    /**
     * Defines parameters to all affine tranformations and execute them in order
     * @param xScale x-scaling factor
     * @param yScale y-scaling factor
     * @param xTransl x-translation factor
     * @param yTransl y-scaling factor
     * @param angle angle of rotation
     * @param diraction flag which specify direction of rotation
     */
	
	public void affineTranform(double xScale, double yScale, float xTransl, float yTransl, int angle, boolean  direction){
		this.scale(xScale, yScale);
		this.translate(xTransl, yTransl);
		this.rotate(angle, direction);
	}
    /**
     * Scaling image
     * @param xScale x-scaling factor
     * @param yScale y-scaling factor
     */
	public void scale(double xScale, double yScale) {
		int jj, ii;
		double scaleMatrix[][] = new double[2][2];	
		scaleMatrix[0][0] = xScale; //Initialization of scale matrix
		scaleMatrix[0][1] = 0;
		scaleMatrix[1][0] = 0;
		scaleMatrix[1][1] = yScale;
	
		for (int i = 1; i < input.getHeight(); i++) {
			ii = (int) (i * scaleMatrix[1][1]); // compute new values of height
			if (ii >= input.getHeight() || ii < 0)
				continue; //continue if height is out of range of input
			for (int j = 1; j < input.getWidth(); j++) {
				Pixel pixel = new Pixel();
				jj = (int) (j * scaleMatrix[0][0]); // compute new values of width  				
				if (jj >= input.getWidth() || jj < 0)
					continue; //continue if width is out of range of input
				pixel = input.getPixel(jj, ii); //each pixel is multiplied by scale matrix
				outputAfterScaling.getPixel(j, i).setPixel(pixel); //setting new pixel values
			}
		}
	}
    /**
     * Translate image
     * @param xTransl x-translating factor
     * @param yTransl y-translating factor
     */
	public void translate(double xTranslate, double yTranslate) {
		int jj, ii;
		for (int i = 1; i < input.getHeight(); i++) {
			ii = (int) (i - yTranslate); // compute new values of height
			if (ii >= input.getHeight() || ii < 0)
				continue; //continue if height is out of range of input
			for (int j = 1; j < input.getWidth(); j++) {
				Pixel pixel = new Pixel();
				jj = (int) (j - xTranslate); // compute new values of width				
				if (jj >= input.getWidth() || jj < 0)
					continue; //continue if width is out of range of input
				pixel = outputAfterScaling.getPixel(jj, ii);
				outputAfterTranslate.getPixel(j, i).setPixel(pixel); //setting new values to output
			}
		}
	}
    /**
     * Rotates image
     * @param angle angle of rotation
     * @param direction flag which specify direction of rotation
     */
	public void rotate(double angle, boolean direction) {
		int jj,ii;
		double rot[][] = new double[2][2];  //rotation matrix
		double inverseRot[][] = new double[2][2]; //inverse rotation matrix 
		rot[0][0] = Math.cos(Math.toRadians(angle)); //initializing values of matrix in case of angle
		rot[0][1] = -Math.sin(Math.toRadians(angle));
		rot[1][0] = Math.sin(Math.toRadians(angle));
		rot[1][1] = Math.cos(Math.toRadians(angle));

		inverseRot[0][0] = Math.cos(Math.toRadians(angle)); //initializing values of inverse matrix in case of angle
		inverseRot[0][1] = Math.sin(Math.toRadians(angle));
		inverseRot[1][0] = -Math.sin(Math.toRadians(angle));
		inverseRot[1][1] = Math.cos(Math.toRadians(angle));

		for (int i = 1; i < input.getHeight(); i++) {
			for (int j = 1; j < input.getWidth(); j++) {
				Pixel pixel = new Pixel();
				if (direction) { //in case of chosen direction rotate in certain direction
					jj = (int) (j * rot[0][0] + i * rot[0][1]);
					ii = (int) (j * rot[1][0] + i * rot[1][1]);
				} else {
					jj = (int) (j * inverseRot[0][0] + i * inverseRot[0][1]);
					ii = (int) (j * inverseRot[1][0] + i * inverseRot[1][1]);
				}
				if (jj >= input.getWidth() || jj < 0)
					continue;         //check if width is in range of input picture size
				if (ii >= input.getHeight() || ii < 0)
					continue;   //check if height is in range of input picture size
				pixel = outputAfterTranslate.getPixel(jj, ii);
				outputAfterRotation.getPixel(j, i).setPixel(pixel);
			}
		}
	}
    /**
     * Save the result
     * @param path path of the file to save
     * @param type determines if result should be normalized and saved as an image or as a text
     */
	@Override
	public void saveData(String path, Integer type) throws IOException {
		outputAfterRotation.saveData(path, input.getType()); //save data after all affine transformations
	}
    /**
     * Get image type
     * @return integer code of image type
     */
	@Override
	public int getType() {
		return input.getType();
	}
}
