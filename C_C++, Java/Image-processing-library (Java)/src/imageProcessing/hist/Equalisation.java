package imageProcessing.hist;

import imageProcessing.*;
import java.util.ArrayList;

public class Equalisation {

    // class containing static methods to equalize histogram

    /**
     * Histogram equalisation on image.
     * @param original The ImageFile image on which operation is performed
     * @return ImageFile image after operation
     */

    public static ImageFile histogramEqualization(ImageFile original) {

        int red;
        int green;
        int blue;
        int alpha;
        Pixel newPixel;

        // Get the Lookup table for histogram equalization
        ArrayList<int[]> histLUT = histogramEqualizationLUT(original);

        // Create new ImageFile which will be returned from function after operations
        ImageFile histogramEQ = new ImageFile(original.getWidth(), original.getHeight());

        for(int i=0; i<original.getWidth(); i++) {          // loop by width size of image
            for(int j=0; j<original.getHeight(); j++) {     // loop by height size of image

                // Get pixels by R, G, B, A
                alpha = original.getPixel(i, j).getAlpha();
                red = original.getPixel(i, j).getRed();
                green = original.getPixel(i, j).getGreen();
                blue = original.getPixel(i, j).getBlue();

                // Set new pixel values using the histogram lookup table
                red = histLUT.get(0)[red];      //red channel
                green = histLUT.get(1)[green];  //green channel
                blue = histLUT.get(2)[blue];    //blue channel

                // Return back to original format
                newPixel = new Pixel(red, green, blue, alpha);

                // Write pixels into image
                histogramEQ.getPixel(i, j).setPixel(newPixel);
            }
        }

        return histogramEQ;
    }

    /**
     * Creating LUT table for equalisation operation.
     * @param input The ImageFile image on which operation is performed
     * @return ArrayList<int[]> containing calculated values
     */

    private static ArrayList<int[]> histogramEqualizationLUT(ImageFile input) {  // Get the histogram equalization lookup table for separate R, G, B channels

        ArrayList<int[]> imageLUT = new ArrayList<int[]>(); // Create the lookup table
        ArrayList<int[]> hist = new Histogram(input).getHistogram();   // get original histogram of image

        // Fill the lookup table
        int[] rhistogram = new int[256];    // red channel table
        int[] ghistogram = new int[256];    // green channel table
        int[] bhistogram = new int[256];    // blue channel table

        for(int i=0; i<rhistogram.length; i++) rhistogram[i] = 0;   //initialization using 0
        for(int i=0; i<ghistogram.length; i++) ghistogram[i] = 0;   //initialization using 0
        for(int i=0; i<bhistogram.length; i++) bhistogram[i] = 0;   //initialization using 0

        long sumr = 0;
        long sumg = 0;
        long sumb = 0;

        float scale_factor = (float) (255.0 / (input.getWidth() * input.getHeight()));  // Calculate the scale factor

        // creating LUT for red channel
        for(int i=0; i<rhistogram.length; i++) {
            sumr += hist.get(0)[i];                 // sum all red values
            int valr = (int) (sumr * scale_factor); // multiply by scale factor
            if(valr > 255) {                        // prevent going outer range
                rhistogram[i] = 255;
            }
            else rhistogram[i] = valr;              // assign calculated value

            // creating LUT for green channel
            sumg += hist.get(1)[i];                   // sum all red values
            int valg = (int) (sumg * scale_factor);     // multiply by scale factor
            if(valg > 255) {                            // prevent going outer range
                ghistogram[i] = 255;
            }
            else ghistogram[i] = valg;                  // assign calculated value

            // creating LUT for blue channel
            sumb += hist.get(2)[i];                  // sum all red values
            int valb = (int) (sumb * scale_factor);   // multiply by scale factor
            if(valb > 255) {                        // prevent going outer range
                bhistogram[i] = 255;
            }
            else bhistogram[i] = valb;              // assign calculated value
        }

        // adding to LUT array
        imageLUT.add(rhistogram);
        imageLUT.add(ghistogram);
        imageLUT.add(bhistogram);

        return imageLUT;
    }
}