package imageProcessing.hist;

/**
 * Created by Mistrzu on 22.05.2017.
 */
import imageProcessing.ImageFile;
import java.util.ArrayList;

public class Histogram
{
    private ArrayList<int[]> histogram;         // histogram items array
    private ImageFile image;                    // original image

    public Histogram(ImageFile input){
        histogram = new ArrayList<int[]>();     // histogram initialization
        image = input;                          // image initialization
        createHistogram();                      // calculate histogram when object is created
    }

    Histogram(){                                // default constructor
        histogram = new ArrayList<int[]>();
        image = null;
    }

    private void createHistogram() {            // function creating histogram of image

        int[] rhistogram = new int[256];        // red color table
        int[] ghistogram = new int[256];        // green color table
        int[] bhistogram = new int[256];        // blue color table

        for(int i=0; i<rhistogram.length; i++) rhistogram[i] = 0; // initialization using 0
        for(int i=0; i<ghistogram.length; i++) ghistogram[i] = 0; // initialization using 0
        for(int i=0; i<bhistogram.length; i++) bhistogram[i] = 0; // initialization using 0

        for(int i=0; i<image.getWidth(); i++) {        // image width loop
            for(int j=0; j<image.getHeight(); j++) {   // image height loop

                int red = image.getPixel(i, j).getRed();        // get red value from all pixels
                int green = image.getPixel(i, j).getGreen();    // get green value from all pixels
                int blue = image.getPixel(i, j).getBlue();      // get blue value from all pixels

                // Increase the values of colors
                rhistogram[red]++; ghistogram[green]++; bhistogram[blue]++;

            }
        }

        histogram.add(rhistogram);  // add red vector to histogram array
        histogram.add(ghistogram);  // add green vector to histogram array
        histogram.add(bhistogram);  // add blue vector to histogram array
    }

    public ArrayList<int[]> getHistogram(){
        return histogram;
    }   // getter to access histogram values
}
