package imageProcessing.morph;

import imageProcessing.ImageFile;


public class Opening extends MorphologicalProcessing{
    /**
     * Opening on binary image.
     * @param img The image on which dilatation operation is performed
     * @return Image after operation
     */
    public static ImageFile binary(ImageFile img) {
        //Opening = erosion + dilatation
        ImageFile eroded = Erosion.binary(img);
        return Dilatation.binary(eroded);
    }
}
