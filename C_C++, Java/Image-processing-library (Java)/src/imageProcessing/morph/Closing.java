package imageProcessing.morph;

import imageProcessing.ImageFile;


public class Closing extends MorphologicalProcessing{
    /**
     * Dilatation on binary image.
     * @param img The image on which dilatation operation is performed
     * @return Image after operation
     */
    public static ImageFile binary (ImageFile img) {
        //Closing = dilatation + erosion
        ImageFile dil = Dilatation.binary(img);
        return Erosion.binary(dil);
    }
}
