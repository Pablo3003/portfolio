package imageProcessing.morph;

import imageProcessing.ImageFile;
import imageProcessing.Pixel;


class MorphologicalProcessing {
    protected static final int WHITE = 255;
    protected static final int BLACK = 0;

    protected static ImageFile generateFile(int[] px, int width, int height) {
        ImageFile out = new ImageFile(width,height);

        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                int v = px[x+y*width];
                out.getPixel(x,y).setPixel(new Pixel(v, v, v, 255));
            }
        }
        return out;
    }
}
