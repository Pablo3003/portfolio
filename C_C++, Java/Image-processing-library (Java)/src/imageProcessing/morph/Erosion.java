package imageProcessing.morph;

import imageProcessing.*;

import java.io.IOException;


public class Erosion  extends MorphologicalProcessing {


    /**
     * Erosion on binary image.
     * @param img The image on which erosion operation is performed
     * @return Image after operation
     */
    public static ImageFile binary(ImageFile img) {
        final int width = img.getWidth();
        final int height = img.getHeight();

        int output[] = new int[width * height];


        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                if(img.getPixel(x,y).getRed() == BLACK){
                    boolean flag = false;
                    for(int ty = y - 1; ty <= y + 1 && !flag; ty++){
                        for(int tx = x - 1; tx <= x + 1 && !flag; tx++){
                            if(ty >= 0 && ty < height && tx >= 0 && tx < width){
                                if(img.getPixel(tx,ty).getRed() != BLACK){
                                    flag = true;
                                    output[x+y*width] = WHITE;
                                }
                            }
                        }
                    }
                    if(!flag){
                        output[x+y*width] = BLACK;
                    }
                }else{
                    output[x+y*width] = WHITE;
                }
            }
        }




        return generateFile(output,width,height);
    }

    /**
     * Erosion on greyscale image.
     * @param img The image on which erosion operation is performed
     * @return Image after operation
     */
    public static ImageFile greyscale(ImageFile img) {
        final int width = img.getWidth();
        final int height = img.getHeight();

        int buff[];

        int output[] = new int[width*height];

        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                buff = new int[9];
                int i = 0;
                for(int ty = y - 1; ty <= y + 1; ty++){
                    for(int tx = x - 1; tx <= x + 1; tx++){
                        if(ty >= 0 && ty < height && tx >= 0 && tx < width){
                            //pixel under the mask
                            buff[i] = img.getPixel(tx, ty).getRed();
                            i++;
                        }
                    }
                }

                //sort buff
                java.util.Arrays.sort(buff);

                //save lowest value
                output[x+y*width] = buff[9-i];
            }
        }


        return MorphologicalProcessing.generateFile(output,width,height);
    }

}
