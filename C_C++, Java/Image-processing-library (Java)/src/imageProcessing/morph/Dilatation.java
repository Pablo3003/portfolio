package imageProcessing.morph;

import imageProcessing.ImageFile;
import imageProcessing.Pixel;



public class Dilatation extends MorphologicalProcessing{
    /**
     * Dilatation on binary image.
     * @param img The image on which dilatation operation is performed
     * @return Image after operation
     */
    public static ImageFile binary(ImageFile img){

        final int width = img.getWidth();
        final int height = img.getHeight();

        int output[] = new int[width * height];


        int reverseValue = 255;

        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                if(img.getPixel(x, y).getRed() == BLACK){
                    boolean flag = false;
                    for(int ty = y - 1; ty <= y + 1 && !flag; ty++){
                        for(int tx = x - 1; tx <= x + 1 && !flag; tx++){
                            if(ty >= 0 && ty < height && tx >= 0 && tx < width){
                                if(img.getPixel(tx, ty).getRed() != BLACK){
                                    flag = true;
                                    output[x+y*width] = reverseValue;
                                }
                            }
                        }
                    }
                    if(!flag){
                        output[x+y*width] = BLACK;
                    }
                }else{
                    output[x+y*width] = reverseValue;
                }
            }
        }

        return generateFile(output,width,height);
    }

    /**
     * Dilatation on greyscale image.
     * @param img The image on which dilatation operation is performed
     * @return Image after operation
     */
    public static ImageFile greyscale(ImageFile img) {
        int width = img.getWidth();
        int height = img.getHeight();

        int buff[];

        int output[] = new int[width*height];

        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                buff = new int[9];
                int i = 0;
                for(int ty = y - 1; ty <= y + 1; ty++){
                    for(int tx = x - 1; tx <= x + 1; tx++){
                        if(ty >= 0 && ty < height && tx >= 0 && tx < width){
                            buff[i] = img.getPixel(tx, ty).getRed();
                            i++;
                        }
                    }
                }

                //sort buff
                java.util.Arrays.sort(buff);

                //save highest value
                output[x+y*width] = buff[8];
            }
        }

        return MorphologicalProcessing.generateFile(output,width,height);
    }

}
