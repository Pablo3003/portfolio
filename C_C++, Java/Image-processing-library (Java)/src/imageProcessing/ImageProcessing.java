package imageProcessing;

import java.io.IOException;


public interface ImageProcessing {
    /**
     * Load an image.
     * @param path path of the file to load
     */
    public void loadData(String path) throws IOException;
    /**
     * Save the result
     * @param path path of the file to save
     * @param type type of the image to save
     */
    public void saveData(String path, Integer type) throws IOException;
    /**
     * Get image type
     * @return integer code of image type
     */
    public int getType();
}
