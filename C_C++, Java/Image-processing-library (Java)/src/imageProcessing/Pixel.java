package imageProcessing;


public class Pixel {
    private Integer red;               // red intensity
    private Integer green;             // green intensity
    private Integer blue;              // blue intensity
    private Integer alpha;             // alpha
    Pixel() {
        red = new Integer(0);       // red color initializer
        green = new Integer(0);     // green color initializer
        blue = new Integer(0);      // blue color initializer
        alpha = new Integer(255);   // alpha initializer
    }
    public Pixel(Integer red, Integer green, Integer blue, Integer alpha) {
        this.red = red;                     // assign red value
        this.green = green;                 // assign green
        this.blue = blue;                   // assign blue
        this.alpha = alpha;                 // assign alpha
    }

    public Integer getRed() { return red; }       // red getter
    public Integer getGreen() { return green; }   // green getter
    public Integer getBlue() { return blue; }     // blue getter
    public Integer getAlpha() { return alpha; }     // blue getter

    public void setRed(Integer red) { this.red=red; }         // red setter
    public void setGreen(Integer green) { this.green=green; } // green setter
    public void setBlue(Integer blue) { this.blue=blue; }     // blue getter
    public void setAlpha(Integer alpha) { this.alpha=alpha; }     // blue getter

    public void setPixel(Pixel px) {
        red = px.getRed();                  //  set pixel values from given pixel
        green = px.getGreen();
        blue = px.getBlue();
        alpha = px.getAlpha();
    }

    public void setAllValues(Integer val) {     // set all colors same value
        red = val;
        green = val;
        blue = val;
    }
}
