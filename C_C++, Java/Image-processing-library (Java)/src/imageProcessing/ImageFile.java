package imageProcessing;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class ImageFile implements ImageProcessing {
    private Pixel[][] pixels;                                       // pixels of an image
    private Integer width = new Integer(0);
    private Integer height = new Integer(0);

    Integer imageType;                                              // image type
    @Override
    public void loadData(String path) throws IOException {
        File input = new File(path);                                // file to open
        BufferedImage file = null;
        file = ImageIO.read(input);                                 // loading to buffer
        imageType = new Integer(file.getType());                    // get type
        width = file.getWidth();
        height = file.getHeight();
        pixels = new Pixel[width][height];
        for (int h=0; h<height; h++)
            for (int w=0; w<width; w++)                   // create pixels objects
                pixels[w][h]=new Pixel();
        for (int h=0; h<height; h++) {
            for (int w=0; w<width; w++) {
                int temp = file.getRGB(w,h);
                pixels[w][h].setAlpha(((temp>>24) & 0xff));
                pixels[w][h].setRed(((temp>>16) & 0xff));
                pixels[w][h].setGreen(((temp>>8) & 0xff));
                pixels[w][h].setBlue(((temp) & 0xff));              // set pixels values
            }
        }
    }
    @Override
    public void saveData(String path, Integer type) throws IOException{
        File output = new File(path+".png");                                    // output file
        BufferedImage file = new BufferedImage(pixels.length, pixels[0].length, type);
        for (int h=0; h<height; h++) {
            for (int w=0; w<width; w++) {
                file.setRGB(w,h, new Color(pixels[w][h].getRed(),pixels[w][h].getGreen(),pixels[w][h].getBlue()).getRGB()); // set pixels
            }
        }
        ImageIO.write(file,"png", output);  // write to file

    }

    public int getType() { return imageType; }              // get type

    public Pixel getPixel(Integer width, Integer height) {  // get pixel of an image
        return pixels[width][height];
    }

    public Integer getWidth() { return width; }             // get image width

    public Integer getHeight() { return height; }             // get image height

     ImageFile() {};                                         // default constructor - use only when loading file

    public ImageFile(Integer width, Integer heigth) {              // second constructor - use to create image to save
        this.width=width;
        this.height=heigth;
        pixels = new Pixel[width][heigth];
        for (int h=0; h<heigth; h++)
            for (int w=0; w<width; w++)
                pixels[w][h]=new Pixel();
    }
    public ImageFile(String path) throws IOException{
        loadData(path);
    }

    public static int bounds(double value, int min, int max)    // function to keep values on range from min to max
    {
        if(value<min)
            return min;
        if(value>max)
            return max;
        return (int)value;
    }
}
