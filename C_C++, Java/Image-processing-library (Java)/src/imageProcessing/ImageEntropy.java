package imageProcessing;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;


public class ImageEntropy implements ImageProcessing {
    private ImageFile input;    // input file
    private ImageFile output;   // output normalized image
    private Double[][] entropyValue;    // entropy values
    /**
     * Create ImageEntropy object and allocate memory for input, and read an image
     * @param path path of the file to load
     */
    public ImageEntropy(String path) throws IOException {
        input = new ImageFile();    // allocating memory for input image
        loadData(path);             // loading input
    }

    /**
     * Create ImageEntropy object and allocate memory for input
     */
    ImageEntropy() {
        input = new ImageFile();
    }   // allocating memory

    public void entropy(int window) throws EvenWindowException{
        if( window % 2 == 0)    // window must be odd
            throw new EvenWindowException();
        int window_squared=window*window;   // squared window for calculations
        int in_w = input.getWidth();
        int in_h = input.getHeight();       // input image size
        double entropy;
        double prob;
        Integer[][] levels = new Integer[in_w][in_h];   // array for levels of brightness
        for(int i=0; i<in_w; i++)
            for(int j=0; j<in_h; j++)
                levels[i][j]=new Integer(input.getPixel(i,j).getRed()); // all values are the same so lets take red (colud be green and blue as well)
        int accessx;
        int accessy;

        for(int i=0; i<in_h; i++)  // for each row
        {
            for(int j=0; j<in_w; j++)   // for each pixel
            {
                entropy=0;
                int[] values = new int[256];
                Arrays.fill(values,0);
                for(int k=-(window/2); k<=window/2; k++)    // for each row in mask
                {
                    accessy = i+k;  // we're moving on every row window
                    accessy=ImageFile.bounds(accessy,0,in_h-1); // value must me in bounds
                    for(int l=-(window/2); l<=window/2; l++)    // for each pixel in mask
                    {
                        accessx = j+l; // we're moving on every column window
                        accessx=ImageFile.bounds(accessx,0,in_w-1); // value must be in bounds
                        values[levels[accessx][accessy]]++; // lets take this level and increase number of it's apperiences
                    }
                }
                for(int b=0; b<256; b++)
                {
                    prob = values[b]/ (double)(window_squared); // probability of color on position in our window
                    if(prob<0.00001)    // if is 0 then it is unnecessary
                        continue;
                    entropy+=(Math.log10(prob)/Math.log10(2))*prob; // else increase our pixel entropy
                }
                entropyValue[j][i]=-entropy;    // and take it's negative
            }
        }
    }

    @Override
    /**
     * Load an image.
     * @param path path of the file to load
     */
    public void loadData(String path) throws IOException {
        input.loadData(path);   // just load image from path
        int in_w = input.getWidth();    // get size
        int in_h = input.getHeight();
        output = new ImageFile(in_w,in_h);  // prepare output image
        entropyValue = new Double[in_w][in_h];  // allocate array for entropy values
        for(int i=0; i<in_w; i++)
            for(int j=0; j<in_h; j++)
                entropyValue[i][j]=new Double(0);   // set it to 0
    }
    @Override
    /**
     * Save the result
     * @param path path of the file to save
     * @param asNormalizedImage determines if result should be normalized and saved as an image or as a text
     */
    public void saveData(String path, Integer asNormalizedImage) throws IOException {
        if(asNormalizedImage==0) {                                      // if we want it as text file
            PrintWriter writer = new PrintWriter(path, "UTF-8");   // prepare writer

            for(int i=0; i<output.getHeight(); i++) {                   // take every row
                for(int j=0; j<output.getWidth(); j++)                  // and column
                                {
                                    writer.print(entropyValue[j][i] + " "); // save it in file
                                }
                writer.println();   // separate rows
            }
            writer.close(); // close file
        }
        if(asNormalizedImage!=0) {  // if we want it as an image
            ArrayList<Double> list = new ArrayList<>();
            ArrayList<Double> list2 = new ArrayList<>();    // prepare list which will help us get min and max values to get factor
            for(int i=0; i<output.getWidth(); i++) {
                list.add(Collections.min(Arrays.asList(entropyValue[i])));
                list2.add(Collections.min(Arrays.asList(entropyValue[i])));
            }
            double min = Collections.min(list);
            double max = Collections.max(list2);    // max and min values of entropy
            double step = 255/max-min;  // multiplier to get values from 0 to 255
            int val;
            for(int i=0; i<output.getWidth(); i++)
                for(int j=0; j<output.getHeight(); j++)
                {
                    val = ImageFile.bounds((entropyValue[i][j]*step),0,255);    // we need our multiplied value in bounds
                    output.getPixel(i,j).setAllValues(val); // set pixel in output image
                }
            output.saveData(path,input.getType());  // save image
        }

    }
    @Override
    /**
     * Get image type
     * @return integer code of image type
     */
    public int getType() {
        return input.getType();
    }
}
