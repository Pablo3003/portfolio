import imageProcessing.*;
import imageProcessing.gauss.GaussianBlur;
import imageProcessing.hist.Equalisation;
import imageProcessing.morph.Dilatation;
import imageProcessing.morph.Erosion;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        try {
            ImageFile file = new ImageFile("images/lena.png");
            //ImageFile out2 = Dilatation.greyscale(file);
            //ImageFile out1 = Erosion.greyscale(file);

            /*ImageEntropy entropy = new ImageEntropy("images/mono.bmp");
            ImageResize resize = new ImageResize("images/lena.png");
            resize.resize(2);
            resize.saveData("images/outResize",1);*/
            /*try{
                entropy.entropy(3);
                entropy.saveData("images/outEntropy",0);
            }catch (EvenWindowException e){System.out.println(e);};*/


            AffineTransform obj = new AffineTransform("images/lena.png");
			Kirsch kirsch = new Kirsch("images/lena.png");
          //  Equalisation hist = new Equalisation("images/mono1.png");
            //Histogram hist = new Histogram(file);
            ImageFile out3 = Equalisation.histogramEqualization(file);
            out3.saveData("images/out3",1);
            ImageFile out4 = GaussianBlur.getBluredImg(file, 7);
            out4.saveData("images/out4", 1);
            obj.affineTranform(1.5, 0.7, 50, 50, 25, true);
			obj.saveData("images/outAffine", 1);
			
			kirsch.KirschMask();

            //out1.saveData("images/out1",1);
            //out2.saveData("images/out2",1);
            //obj.saveData("images/outAffine", 1);
            kirsch.saveData("images/outKirsch", 1);
            
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
}

