const yargs = require('yargs');	// library to smooth parse console arguments
const axios = require('axios');	// library like request but with promises



//-----setting -a parameter to fetch address------//
const argv = yargs					
  .options({
    a: {
      alias: 'address',
      describe: 'Address to fetch weather for',
      string: true
    }
  })
  .argv;
  
if (argv.address === ""){			// return from script when no address given
	console.log("empty address");
	return;
}
  
var addressEncoded = encodeURIComponent(argv.address); // encode address to URL format
var geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${addressEncoded}`;	// URL with to googlemaps api with specified location

axios.get(geocodeUrl).then((response) => {		// get request to google api and when response received callback
  if (response.data.status === 'ZERO_RESULTS') {	// if status zero results then probably wrong address
    throw new Error('Unable to find that address.');	// throw error 
  }

  var latitude = response.data.results[0].geometry.location.lat;	// get latitude from response object
  var longitude = response.data.results[0].geometry.location.lng;	// get longitude from response object
  
  var weatherUrl = `https://api.forecast.io/forecast/7b2d8d0da3fa7784223a455168f89b0d/${latitude},${longitude}`;	// request to weather api
  
  console.log(response.data.results[0].formatted_address); // print exact address with nice format
  console.log("-------------------------------------------");
  
  return axios.get(weatherUrl);	// promise chain - get request to weather api
  
}).then((response) => {	// when response received callback 

  var summary = response.data.currently.summary;
  var temperature = (response.data.currently.temperature-32)*5/9;	// temerature from fahrenthait to celcsius
  var pressure = response.data.currently.pressure;			// pressure
  var wind = response.data.currently.windSpeed;			// wind speed
  
  console.log(`Pogoda na dziś to: ${summary}.`);
  console.log(`Temperatura w stopniach Celciusza to: ${temperature.toFixed(2)}.`);
  console.log(`Ciśnienie powietrza ma wartość: ${pressure}.`);
  console.log(`Predkosc wiatru to: ${wind}.`);
  
}).catch((e) => {				// catch errors
  if (e.code === 'ENOTFOUND') {		// if code ENOTFOUND then probably api server not working
    console.log('Unable to connect to API servers.');
  } else {
    console.log(e.message);		// else errors
  }
});
