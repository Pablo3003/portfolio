from tkinter import *
from tkinter import filedialog
import re
from Graph import Graph

class mainWindow(object):
    window = None
    o_button = None
    s_button = None
    g_listbox = None
    g_scrollbar = None

    lines = []
    measurements = []
    readed = False
    step = 0
    start = 0
    stop = 0

    def __init__(self, name):
        self.window = Tk()
        self.window.wm_title(name)
        self.window.geometry("165x220")
        self.window.configure(background='lavender')

        self.o_button = Button(self.window, text="Open", command=self.openf, width=6)
        self.s_button = Button(self.window, text="Show", command=self.showg, width=6)
        self.o_button.grid(row=0, column=1, pady=5, padx=(20, 0), sticky=W)
        self.s_button.grid(row=0, column=1, pady=5, padx=(0, 20), sticky=E)
        self.s_button.config(state="disabled")

        self.g_listbox = Listbox(self.window, height=10, width=20)
        self.g_listbox.grid(row=1, column=1, pady=5, padx=20)
        self.window.resizable(False, False)
        self.window.mainloop()

    def openf(self):
        file = filedialog.askopenfile()
        if self.readed is True:
            self.g_listbox.delete(0, self.g_listbox.size())
            self.lines = []
            self.measurements = [[]]
            self.readed = False
        for line in file:
            self.lines.append(line)
        r = re.compile(".*STRT\.M.*")
        result = filter(r.match, self.lines)
        result0 = result.__next__()
        self.start = float(result0.split()[1][:-1])
        r = re.compile(".*STOP\.M.*")
        result = filter(r.match, self.lines)
        result0 = result.__next__()
        self.stop = float(result0.split()[1][:-1])
        r = re.compile(".*STEP\.M.*")
        result = filter(r.match, self.lines)
        result0 = result.__next__()
        self.step = float(result0.split()[1][:-1])

        r = re.compile("~A")
        types = filter(r.match, self.lines)
        types0 = next(types)
        val_list = self.lines[self.lines.index(types0)+1:]
        self.g_listbox.config(state="normal")
        self.g_listbox.insert(END, *types0.split()[2:])
        self.g_listbox.selection_set(0)
        self.s_button.config(state="normal")

        for i in range(self.g_listbox.size()+1):
            self.measurements.append([])
        if val_list[0][0] is '#':
            val_list.remove(val_list[0])
        for values in val_list:
            x = values.split()
            for n in range(0, self.g_listbox.size()+1):
                if float(x[n]) <= 0.01:
                    if len(self.measurements[n]) is not 0:
                        x[n] = min(self.measurements[n])
                    else:
                        x[n] = 0.01
                self.measurements[n].append(float(x[n]))
        self.readed = True

    def showg(self):
        b = Graph(self.measurements[self.g_listbox.curselection()[0]+1], self.measurements[0])
