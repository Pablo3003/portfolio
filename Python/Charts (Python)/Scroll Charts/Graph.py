from math import log10
import matplotlib
import matplotlib.pyplot as plt
from VertSlider import VertSlider
from matplotlib.widgets import RadioButtons
import matplotlib.gridspec as gridspec


class Graph(object):

    def __init__(self, x, y):
        matplotlib.rcParams['toolbar'] = 'None'
        self.state = True
        self.fig, self.ax = plt.subplots()
        plt.subplots_adjust(left=0.1, bottom=0.3)
        self.l, = plt.plot(x, y)
        self.xmin = sorted(x)[0]
        self.xmax = sorted(x)[-1]
        self.ymin = sorted(y)[0]
        self.ymax = sorted(y)[-1]
        plt.axis([self.xmin, self.xmax, self.ymin, self.ymin+(self.ymax-self.ymin)/16])
        self.axframe = plt.axes([0.91, 0.3, 0.015, 0.58])
        self.sframe = VertSlider(self.axframe, '', 0, self.ymax-(self.ymin+(self.ymax-self.ymin)/16), valinit=0,
                                 valfmt='%d', facecolor='lavender')
        self.sframe.valtext.set_visible(False)
        self.sframe.on_changed(self.update)

        self.fig.canvas.callbacks.connect('motion_notify_event', self.callback)
        self.fig.canvas.set_window_title('Scroll Charts')
        self.fig.patch.set_facecolor('lavender')
        self.ax.grid(True, which="both")

        rax = plt.axes([0.4, 0.1, 0.2, 0.1])
        self.radio = RadioButtons(rax, ('linear', 'logarythmic'))
        self.radio.on_clicked(self.radiof)

        plt.show()

    def radiof(self, label):
            rstate = {'linear': True, 'logarythmic': False}
            self.state = rstate[label]
            if self.state:
                self.ax.set_xscale('linear')
                self.ax.set_yscale('linear')
            else:
                self.ax.set_xscale('log')
                self.ax.set_yscale('log')
            self.fig.canvas.draw_idle()

    def update(self, val):
        pos = self.sframe.val
        self.ax.axis([self.xmin, self.xmax, self.ymin+pos, self.ymin+(self.ymax-self.ymin)/16+pos])
        self.fig.canvas.draw_idle()

    @staticmethod
    def callback(event):
        if event.xdata is not None and event.ydata is not None and str(event.inaxes) == "Axes(0.25,0.1;0.65x0.8)":
            pass
