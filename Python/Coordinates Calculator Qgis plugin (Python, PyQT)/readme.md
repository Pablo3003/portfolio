

***************************************************************************
Coordinates Calculator

Application allow QGis user to calculate coordinates beetween systems PUWG 1992 and WGS 84.
User can import converted coordinates to QGis and edit it as a shp layer. 
There is possibility to export from main program selected layer to plugin and save changed coordinates as a txt file.

Requirements:

- Creative project proving knowledge of Python
                                
        begin                : 2016-12-01
        copyright            : (C) 2016 by Paweł Marszalec
        email                : pawel.marszalec92@gmail.com
***************************************************************************

![picture](img.png)



