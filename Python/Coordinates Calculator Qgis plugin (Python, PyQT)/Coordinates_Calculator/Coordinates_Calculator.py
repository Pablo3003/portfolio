# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Coordinates_Calculator
                                 A QGIS plugin
 Coordinates_Calculator
                              -------------------
        begin                : 2016-12-06
        copyright            : (C) 2016 by Paweł Marszalec
        email                : pawel.marszalec92@gmail.com
 ***************************************************************************/"""
import os.path
import os
import qgis
from math import *
from osgeo import osr, ogr
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import numpy as np

from Coordinates_Calculator_dialog import Coordinates_CalculatorDialog

import resources



class Coordinates_Calculator:

    def __init__(self, iface):

        self.iface = iface

        self.plugin_dir = os.path.dirname(__file__)

        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'Coordinates_Calculator_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        self.actions = []
        self.menu = self.tr(u'&Coordinates_Calculator')

        self.toolbar = self.iface.addToolBar(u'Coordinates_Calculator')
        self.toolbar.setObjectName(u'Coordinates_Calculator')

        self.input_index = 0
        self.output_index = 0
        self.WGS_flag = 2
        self.PUWG_1992_flag = 2
        self.output_EPSG = 4326
        self.order_flag = [0, 0, 0, 0, 0]
        self.tab_input = []
        self.tab_output = []
        self.index_to_decription = {'nr': 0, 'x_input': 1, 'y_input': 2, 'x_output': 3, 'y_output': 4} # dictionary to better code understand

        self.layers = iface.legendInterface().layers()
        self.dlg = Coordinates_CalculatorDialog()   #GUI object

    def tr(self, message):
        return QCoreApplication.translate('Coordinates_Calculator', message)    #from qgis documentation

    def add_action(         #from qgis documentation
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):

        self.dlg = Coordinates_CalculatorDialog() #GUI object

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):      #method responsible for GUI handling
        icon_path = ':/plugins/Coordinates_Calculator/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Coordinates_Calculator'),
            callback=self.run,
            parent=self.iface.mainWindow())

        self.dlg.input_box.currentIndexChanged.connect(self.input_box_selected)     #connection methods with interface
        self.dlg.output_box.currentIndexChanged.connect(self.output_box_selected)

        self.dlg.load_button.clicked.connect(self.load_coordinates)

        self.dlg.convert_button.clicked.connect(self.select_method)
        self.dlg.refresh_button.clicked.connect(self.layer_refresh)
        self.dlg.load_layer_button.clicked.connect(self.load_layer)
        self.dlg.save_button.clicked.connect(self.save_coordinates)
        self.dlg.import_button.clicked.connect(self.shp_generate)

        self.dlg.coordinat_table.horizontalHeader().sectionClicked.connect(self.sort)
        self.dlg.degree_min_sec_button.clicked.connect(self.change_degree_min_sec)
        self.dlg.degree_button.clicked.connect(self.change_degree)
        self.dlg.m_button.clicked.connect(lambda: self.change_km(1))                # lambda parameter used to pass argument to function change_km in connect
        self.dlg.km_button.clicked.connect(lambda: self.change_km(0))               # lambda parameter used to pass argument to function change_km in connect

    def select_method(self):
        if self.input_index == 0 and self.output_index == 1:
            self.WGS_84_to_1992_convert()
        if self.input_index == 1 and self.output_index == 0:
            self.PUWG_1992_to_WGS_84_convert()

    def input_box_selected(self, index):
        if index == 0:
            self.input_index = 0
            self.dlg.coordinat_table.horizontalHeaderItem(1).setText(u"Longitude [°]")     #changing header of work_table in GUI
            self.dlg.coordinat_table.horizontalHeaderItem(2).setText(u"Latitude [°]")       #changing header of work_table in GUI
        if index == 1:
            self.input_index = 1
            self.dlg.coordinat_table.horizontalHeaderItem(1).setText("X 1992 [m]")          #changing header of work_table in GUI
            self.dlg.coordinat_table.horizontalHeaderItem(2).setText("Y 1992 [m]")          #changing header of work_table in GUI
        if self.input_index == self.output_index:
            self.dlg.convert_button.setDisabled(True)
        else:
            self.dlg.convert_button.setDisabled(False)

    def output_box_selected(self, index):
        if index == 0:
            self.output_index = 0
            self.output_EPSG = 4326             # changing default coordinate system
            self.dlg.coordinat_table.horizontalHeaderItem(3).setText(u"Longitude [°]")  #changing header of work_tabl
            self.dlg.coordinat_table.horizontalHeaderItem(4).setText(u"Latitude [°]")   #changing header of work_tabl
        if index == 1:
            self.output_index = 1
            self.output_EPSG = 2180             # changing default coordinate system
            self.dlg.coordinat_table.horizontalHeaderItem(3).setText("X 1992 [m]")  #changing header of work_tabl
            self.dlg.coordinat_table.horizontalHeaderItem(4).setText("Y 1992 [m]")  #changing header of work_tabl
        if self.input_index == self.output_index:
            self.dlg.convert_button.setDisabled(True)
        else:
            self.dlg.convert_button.setDisabled(False)

    def unload(self):       #deconstuctor
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&Coordinates_Calculator'),
                action)
            self.iface.removeToolBarIcon(action)
        del self.toolbar

    #test = lambda self: QMessageBox.question(self.iface.mainWindow(), 'Continue?', 'Your message here', QMessageBox.Yes, QMessageBox.No)

    def load_coordinates(self):     # loading coordinates from file to table

        load = QFileDialog.getOpenFileName
        name = load(self.dlg, 'chose file with coordinates', '', 'TXT files (*.txt);;ALL Files (*.*)')
        with open(str(name), 'r') as file:
            tab = self.dlg.coordinat_table
            rowI = QTableWidgetItem
            for line in file:
                nr, x_input, y_input = line.split()
                self.tab_input.append([int(nr), float(x_input), float(y_input)])
                tab.insertRow(tab.rowCount())
                tab.setItem(tab.rowCount() - 1, self.index_to_decription['nr'], rowI(nr))
                tab.setItem(tab.rowCount() - 1, self.index_to_decription['x_input'], rowI(x_input))
                tab.setItem(tab.rowCount() - 1, self.index_to_decription['y_input'], rowI(y_input))
        self.dlg.save_button.setDisabled(False)
        self.dlg.import_button.setDisabled(False)
        self.dlg.input_box.setDisabled(False)
        self.dlg.output_box.setDisabled(False)


    def WGS_84_to_1992_convert(self):
        row = 0
        RI = QTableWidgetItem
        tab = self.dlg.coordinat_table
        self.tab_output = []
        convert_func = self.convert(1)
        for nr, x_input, y_input in self.tab_input:
            x_output, y_output = convert_func(x_input, y_input)
            self.tab_output.append([nr, x_output, y_output])
            tab.setItem(row, self.index_to_decription['x_output'], RI(str(x_output)))
            tab.setItem(row, self.index_to_decription['y_output'], RI(str(y_output)))
            row += 1
        self.WGS_flag = 0
        self.PUWG_1992_flag = 1
        self.dlg.degree_min_sec_button.setDisabled(False)
        self.dlg.km_button.setDisabled(False)
        self.dlg.m_button.setChecked(True)
        self.dlg.degree_button.setChecked(True)
        self.dlg.m_button.setDisabled(True)
        self.dlg.degree_button.setDisabled(True)
        self.change_degree()


    @staticmethod
    def convert(indicator):

        L0_stopnie = 19.0
        m0 = 0.9993
        x0 = -5300000.0
        y0 = 500000.0
        R0 = 6367449.14577
        Snorm = 2.0E-6

        def puwg92_to_wgs84_calc(x_input, y_input):
            xo_prim = 5765181.11148097
            b0 = 5760000
            b1 = 500199.26224125
            b2 = 63.88777449
            b3 = -0.82039170
            b4 = -0.13125817
            b5 = 0.00101782
            b6 = 0.00010778
            c2 = 0.0033565514856
            c4 = 0.0000065718731
            c6 = 0.0000000176466
            c8 = 0.0000000000540
            Xgk = (x_input - x0) / m0
            Ygk = (y_input - y0) / m0
            Zreal = (Xgk - xo_prim) * Snorm
            Zimag = (Ygk * Snorm) * 1j
            Z = Zreal + Zimag
            Zmerc = b0 + Z * (b1 + Z * (b2 + Z * (b3 + Z * (b4 + Z * (b5 + Z * b6)))))
            Xmerc = Zmerc.real
            Ymerc = Zmerc.imag
            alfa = Xmerc / R0
            beta = Ymerc / R0
            w = 2.0 * atan(exp(beta)) - pi / 2.0
            fi = asin(cos(w) * sin(alfa))
            d_lambda = atan(tan(w) / cos(alfa))
            B = fi + c2 * sin(2.0 * fi) + c4 * sin(4.0 * fi) + c6 * sin(6.0 * fi) + c8 * sin(8.0 * fi)
            dL = d_lambda
            y_output = round((B / pi * 180.0), 6)
            dL_stopnie = dL / pi * 180.0
            x_output = round(dL_stopnie + L0_stopnie, 6)

            return x_output, y_output

        def WGS_84_to_1992_calc(x_input, y_input):
            e = 0.0818191910428
            xo = 5760000.0
            a0 = 5765181.11148097
            a1 = 499800.81713800
            a2 = -63.81145283
            a3 = 0.83537915
            a4 = 0.13046891
            a5 = -0.00111138
            a6 = -0.00010504
            Bmin = 48.0 * pi / 180.0
            Bmax = 56.0 * pi / 180.0
            dLmin = -6.0 * pi / 180.0
            dLmax = 6.0 * pi / 180.0
            B = y_input * pi / 180.0
            dL_stopnie = x_input - L0_stopnie
            dL = dL_stopnie * pi / 180.0
            if (B < Bmin) or (B > Bmax):
                x_output = "Latitude out of Poland area"
                y_output = "Latitude out of Poland area"
                return x_output, y_output
            if (dL < dLmin) or (dL > dLmax):
                x_output = "Longitude out of Poland area"
                y_output = "Longitude out of Poland area"
                return x_output, y_output
            U = 1.0 - e * sin(B)
            V = 1.0 + e * sin(B)
            K = pow((U / V), (e / 2.0))
            C = K * tan(B / 2.0 + pi / 4.0)
            fi = 2.0 * atan(C) - pi / 2.0
            d_lambda = dL
            p = sin(fi)
            q = cos(fi) * cos(d_lambda)
            r = 1.0 + cos(fi) * sin(d_lambda)
            s = 1.0 - cos(fi) * sin(d_lambda)
            XMERC = R0 * atan(p / q)
            YMERC = 0.5 * R0 * log(r / s)
            Zreal = ((XMERC - xo) * Snorm)
            Zimag1 = YMERC * Snorm
            Z = Zreal + Zimag1 * 1j
            Zgk = a0 + Z * (a1 + Z * (a2 + Z * (a3 + Z * (a4 + Z * (a5 + Z * a6)))))
            Xgk = Zgk.real
            Ygk = Zgk.imag

            y_output = round(m0 * Xgk + x0, 3)
            x_output = round(m0 * Ygk + y0, 3)

            return x_output, y_output

        if indicator == 0:
            return puwg92_to_wgs84_calc
        if indicator == 1:
            return WGS_84_to_1992_calc

    def PUWG_1992_to_WGS_84_convert(self):
        row = 0
        RI = QTableWidgetItem
        tab = self.dlg.coordinat_table
        self.tab_output = []
        convert_func = self.convert(0)
        for nr, x_input, y_input in self.tab_input:
            x_output, y_output = convert_func(x_input, y_input)
            self.tab_output.append([nr, x_output, y_output])
            tab.setItem(row, self.index_to_decription['x_output'], RI(str(x_output)))
            tab.setItem(row, self.index_to_decription['y_output'], RI(str(y_output)))
            row += 1
        self.WGS_flag = 1
        self.PUWG_1992_flag = 0
        self.dlg.degree_min_sec_button.setDisabled(False)
        self.dlg.km_button.setDisabled(False)
        self.dlg.m_button.setChecked(True)
        self.dlg.degree_button.setChecked(True)
        self.dlg.m_button.setDisabled(True)
        self.dlg.degree_button.setDisabled(True)
        self.change_km(1)

    def save_coordinates(self):
        save = QFileDialog.getSaveFileName
        name = save(self.dlg, 'Choose output file', '', 'TXT files (*.txt);; ALL (*.*)')
        try:
            file = open(name, 'w')
        except IOError:
            QMessageBox.critical(self, u'Error', u'Error writing to file')
        else:
            for nr, x_output, y_output in self.tab_output:
                file.write('{nr} {x_output} {y_output}\n'.format(**locals()))

    def shp_generate(self):
        shp_driver = ogr.GetDriverByName("ESRI Shapefile")
        shp_file = QFileDialog.getSaveFileName
        shp_path = shp_file(self.dlg, "Choose shp file", '', 'SHP files (*.shp)')
        layer_name = os.path.basename(str(shp_path))[:-4]
        if os.path.exists(str(shp_path)):
            shp_driver.DeleteDataSource(str(shp_path))
        shp = shp_driver.CreateDataSource(str(shp_path))
        spatial_reference = osr.SpatialReference()
        spatial_reference.ImportFromEPSG(self.output_EPSG)
        layer = shp.CreateLayer(layer_name, spatial_reference, ogr.wkbPoint)
        field = ogr.FieldDefn("Nr", ogr.OFTString)
        field.SetWidth(10)
        layer.CreateField(field)
        layer.CreateField(ogr.FieldDefn("x_output", ogr.OFTReal))
        layer.CreateField(ogr.FieldDefn("y_output", ogr.OFTReal))
        for nr, x_output, y_output in self.tab_output:
            point = ogr.Feature(layer.GetLayerDefn())
            point.SetField("Nr", str(nr))
            point.SetField("x_output", x_output)
            point.SetField("y_output", y_output)
            geometry = ogr.Geometry(ogr.wkbPoint)
            geometry.AddPoint(x_output, y_output)
            point.SetGeometry(geometry)
            layer.CreateFeature(point)
        qgis.utils.iface.addVectorLayer(shp_path, layer_name, 'ogr')

    def sort(self, index):
        if self.order_flag[index] == 0:
            self.dlg.coordinat_table.sortByColumn(index, Qt.AscendingOrder)
            self.order_flag[index] = 1
        else:
            self.dlg.coordinat_table.sortByColumn(index, Qt.DescendingOrder)
            self.order_flag[index] = 0

    def change_km(self, indicator):
        def action(indicator, x, y):
            if indicator == 0:
                return x / 1000, y / 1000
            if indicator == 1:
                return x, y

        if self.WGS_flag == 0 and self.PUWG_1992_flag == 1:
            row = 0
            RI = QTableWidgetItem
            tab = self.dlg.coordinat_table
            self.dlg.coordinat_table.horizontalHeaderItem(3).setText("X 1992 [km]")
            self.dlg.coordinat_table.horizontalHeaderItem(4).setText("Y 1992 [km]")
            for nr, x_output, y_output in self.tab_output:
                x_output, y_output = action(indicator, x_output, y_output)
                tab.setItem(row, self.index_to_decription['x_output'], RI(str(x_output)))
                tab.setItem(row, self.index_to_decription['y_output'], RI(str(y_output)))
                row += 1
        if self.WGS_flag == 1 and self.PUWG_1992_flag == 0:
            row = 0
            RI = QTableWidgetItem
            self.dlg.coordinat_table.horizontalHeaderItem(1).setText("X 1992 [km]")
            self.dlg.coordinat_table.horizontalHeaderItem(2).setText("Y 1992 [km]")
            tab = self.dlg.coordinat_table
            for nr, x_input, y_input in self.tab_input:
                x_input, y_input = action(indicator, x_input, y_input)
                tab.setItem(row, self.index_to_decription['x_input'], RI(str(x_input)))
                tab.setItem(row, self.index_to_decription['y_input'], RI(str(y_input)))
                row += 1
        if indicator == 0:
            self.dlg.km_button.setDisabled(True)
            self.dlg.m_button.setDisabled(False)
        if indicator == 1:
            self.dlg.m_button.setDisabled(True)
            self.dlg.km_button.setDisabled(False)

    def change_degree_min_sec(self):
        if self.WGS_flag == 1 and self.PUWG_1992_flag == 0:
            row = 0
            RI = QTableWidgetItem
            tab = self.dlg.coordinat_table
            self.dlg.coordinat_table.horizontalHeaderItem(3).setText(u"Longitude [° \' \"]")
            self.dlg.coordinat_table.horizontalHeaderItem(4).setText(u"Latitude [° \' \"]")
            for nr, x_output, y_output in self.tab_output:
                degree_x = int(x_output)
                min_x = int((modf(x_output)[0]) * 60)
                sec_x = round(modf((modf(x_output)[0]) * 60)[0] / 100 * 60, 2)
                x_output = str(degree_x) + u"° " + str(min_x) + "\' " + str(sec_x) + "\" "
                degree_y = int(y_output)
                min_y = int((modf(y_output)[0]) * 60)
                sec_y = round(modf((modf(y_output)[0]) * 60)[0] / 100 * 60, 2)
                y_output = str(degree_y) + u"° " + str(min_y) + "\' " + str(sec_y) + "\" "
                tab.setItem(row, self.index_to_decription['x_output'], RI(x_output))
                tab.setItem(row, self.index_to_decription['y_output'], RI(y_output))
                row += 1
        if self.WGS_flag == 0 and self.PUWG_1992_flag == 1:
            row = 0
            RI = QTableWidgetItem
            tab = self.dlg.coordinat_table
            self.dlg.coordinat_table.horizontalHeaderItem(1).setText(u"Longitude [° \' \"]")
            self.dlg.coordinat_table.horizontalHeaderItem(2).setText(u"Latitude [° \' \"]")
            for nr, x_input, y_input in self.tab_input:
                min_x, sec_x = divmod(x_input * 3600, 60)
                degree_x, min_x = divmod(min_x, 60)
                x_input = str(degree_x) + u"° " + str(min_x) + "\' " + str(sec_x) + "\" "
                min_y, sec_y = divmod(y_input * 3600, 60)
                degree_y, min_y = divmod(min_y, 60)
                y_input = str(degree_y) + u"° " + str(min_y) + "\' " + str(sec_y) + "\" "
                tab.setItem(row, self.index_to_decription['x_input'], RI(x_input))
                tab.setItem(row, self.index_to_decription['y_input'], RI(y_input))
                row += 1
        self.dlg.degree_min_sec_button.setDisabled(True)
        self.dlg.degree_button.setDisabled(False)

    def change_degree(self):
        if self.WGS_flag == 1 and self.PUWG_1992_flag == 0:
            row = 0
            RI = QTableWidgetItem
            tab = self.dlg.coordinat_table
            self.dlg.coordinat_table.horizontalHeaderItem(3).setText(u"Longitude [°]")
            self.dlg.coordinat_table.horizontalHeaderItem(4).setText(u"Latitude [°]")
            for nr, x_output, y_output in self.tab_output:
                tab.setItem(row, self.index_to_decription['x_output'], RI(str(x_output)))
                tab.setItem(row, self.index_to_decription['y_output'], RI(str(y_output)))
                row += 1
        if self.WGS_flag == 0 and self.PUWG_1992_flag == 1:
            row = 0
            RI = QTableWidgetItem
            tab = self.dlg.coordinat_table
            self.dlg.coordinat_table.horizontalHeaderItem(1).setText(u"Longitude [°]")
            self.dlg.coordinat_table.horizontalHeaderItem(2).setText(u"Latitude [°]")
            for nr, x_input, y_input in self.tab_input:
                tab.setItem(row, self.index_to_decription['x_input'], RI(str(x_input)))
                tab.setItem(row, self.index_to_decription['y_input'], RI(str(y_input)))
                row += 1
        self.dlg.degree_min_sec_button.setDisabled(False)
        self.dlg.degree_button.setDisabled(True)

    def layer_refresh(self):
        self.layers = self.iface.legendInterface().layers()
        layer_list = list();
        for layer in self.layers:
            layer_list.append(layer.name())
            self.dlg.layerBox.clear()
            self.dlg.layerBox.addItems(layer_list)

    def load_layer(self):
        row = 0
        RI = QTableWidgetItem
        tab = self.dlg.coordinat_table
        selected_layer_index = self.dlg.layerBox.currentIndex()
        selected_layer = self.layers[selected_layer_index]
        for feature in selected_layer.getFeatures():
            attr = feature.attributes()
            self.tab_input.append([attr[0],attr[1],attr[2]])
            tab.insertRow(tab.rowCount())
            tab.setItem(tab.rowCount() - 1, self.index_to_decription['nr'], RI(str(attr[0])))
            tab.setItem(tab.rowCount() - 1, self.index_to_decription['x_input'], RI(str(attr[1])))
            tab.setItem(tab.rowCount() - 1, self.index_to_decription['y_input'], RI(str(attr[2])))
            row += 1
        self.dlg.save_button.setDisabled(False)
        self.dlg.import_button.setDisabled(False)
        self.dlg.input_box.setDisabled(False)
        self.dlg.output_box.setDisabled(False)
    run = lambda self: self.dlg.show()



