# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Coordinates_CalculatorDialog
                                 A QGIS plugin
 Coordinates_Calculator
                             -------------------
        begin                : 2016-12-06
        copyright            : (C) 2016 by Paweł Marszalec
        email                : pawel.marszalec92@gmail.com
 ***************************************************************************/"""

import os

from PyQt4 import QtGui, uic

FORM_CLASS, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'Coordinates_Calculator_dialog_base.ui'))


class Coordinates_CalculatorDialog(QtGui.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        super(Coordinates_CalculatorDialog, self).__init__(parent)  # Set up the user interface from Designer.
                                                                    # After setupUI you can access any designer object by doing self.<objectname>, and you can use autoconnect slots - see
        self.setupUi(self)                                          # setting interface for plugin
