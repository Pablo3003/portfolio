# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Coordinates_Calculator
                                 A QGIS plugin
 Coordinates_Calculator
                             -------------------
        begin                : 2016-12-06
        copyright            : (C) 2016 by Paweł Marszalec
        email                : pawel.marszalec92@gmail.com
 ***************************************************************************/

 This script initializes the plugin, making it known to QGIS.
"""



def classFactory(iface):
    """Load Coordinates_Calculator class from file Coordinates_Calculator."""
    from .Coordinates_Calculator import Coordinates_Calculator
    return Coordinates_Calculator(iface)
