(function() {       // own namespace

var chat = {

    //------------------------ connect to server---------------------------------
    connect: function() {

        this.socket = new WebSocket("ws://localhost:8000");     // create new web socket
        this.socket.onmessage = this.displayMessage.bind(this); // on message handler
        this.socket.onclose = this.stopApplication.bind(this); // on close handler

    },

    //------------------------ get all necessary DOM elements---------------------
    init: function() {

        if(!window.WebSocket) return;       // check if web socket available

        this.nameInput = document.querySelector("#userName");
        this.joinButton = document.querySelector("#join");
        this.chatWindow = document.querySelector("#chat");
        this.messageInput = document.querySelector("#message");
        this.submitButton = document.querySelector("#submit");

        this.joinButton.onclick = this.newChatter.bind(this);

        this.connect();     // connect to server

    },

    //--------------------------------create new row when some message on chat-------------//
    newRow: function(dataObject) {

        var chatRow = document.createElement("div"),        // create div
            date = new Date(),                          // new date object
            time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(), // get actual time
            message;

        chatRow.classList.add("chatRow");   // add class chatRow
        if(dataObject.type == "state")      // diffrent row when status and diffrent when message
            message = "<span class='status'>" + dataObject.message + "</span>"; // create message
        else
            message = "<span class='name'>" + dataObject.name + "</span><span class='message'>" + dataObject.message + "</span>"; // create message

        chatRow.innerHTML = "<span class='time'>" + time + "</span>\n" + message; // insert message to HTML

        this.chatWindow.appendChild(chatRow);   // add child to chat
        this.chatWindow.scrollTop = this.chatWindow.scrollHeight; // scroll to last row

    },

    //--------------------------------new person joining chat-------------------------------------//

    newChatter: function(e) {

        var user = this.nameInput.value;

        if(user !== "") {               // if user name not empty
            this.sendData({
                type: "join",
                name: user
            });

            e.target.onclick = null;
            e.target.setAttribute("disabled", "disabled");
            this.nameInput.setAttribute("readonly", "readonly");
            this.submitButton.removeAttribute("disabled");
            this.submitButton.onclick = this.sendMessage.bind(this);
        }
    },

    //--------------------------------sending data-------------------------------------------------------//
    sendData: function(msgObject) {

        var data = JSON.stringify(msgObject);   // object to string before send
        this.socket.send(data);     // sending
    },

    //--------------------------------display new message-----------------------------------------------------//
    displayMessage: function(e) {

        var dataObject = JSON.parse(e.data);    // string to object
        this.newRow(dataObject);                // create new row
    },

    //--------------------------------sending message----------------//
    sendMessage: function() {

        var message = this.messageInput.value;
        if(message !== "") {
            this.sendData({
                type: "message",
                message: message
            });

            this.messageInput.value = "";
        }

    },

    stopApplication: function() {

        this.joinButton.onclick = null;
        this.joinButton.setAttribute("disabled", "disabled");
        this.submitButton.onclick = null;
        this.submitButton.setAttribute("disabled", "disabled");

        this.newRow({
            type: "state",
            message: "Połaczenie z serwerem zostało przerwane."
        });
    }
};

chat.init();

})();