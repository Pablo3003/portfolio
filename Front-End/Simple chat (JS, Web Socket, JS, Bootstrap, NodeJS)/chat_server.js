var ws = require("nodejs-websocket");           // adding web socket module

//------------------------- creating server------------------------------------

var server = ws.createServer((connection) => {   // creating server

    connection.on("text", (data) => {

        var dataObject = JSON.parse(data);          // received string to object

        if(dataObject.type == "join") {         // if join action
            connection.nickName = dataObject.name;

            sendToAll({                         // send to all chatters
                type: "state",
                message: `${connection.nickName} dołączył właśnie do czatu.`
            });
        } else if(dataObject.type == "message") {   // if message action
            sendToAll({
                type: "message",
                name: connection.nickName,
                message: dataObject.message
            });
        }
    });

    connection.on("error", (e) => {            // error handler
        console.log("Nieoczekiwanie przerwano połączenie!");
    });

    connection.on("close", () => {         // close handler
        if(connection.nickName) {
            sendToAll({
                type: "state",
                message: `${connection.nickName} opuścił czat.`
            });
        }
    });

}).listen(8000, "localhost", () => {
    console.log("Serwer jest aktywny!");
});

//--------------------------------------sending to all--------------------------------

function sendToAll(data) {

    var msg = JSON.stringify(data); // change to string

    server.connections.forEach((connection) => {
        connection.sendText(msg); // send for all connections
    });

}