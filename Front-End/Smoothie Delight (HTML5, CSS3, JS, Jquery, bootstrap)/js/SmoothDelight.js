jQuery(function ($) {
    'use strict';

    new WOW().init();

    //-------------------------------sticky navigation--------------------------------------

    $('#menuContainer').addClass('menu');
    var NavY = $('#menuContainer').offset().top;
    console.log(NavY);

    var stickyNav = function(){
        var ScrollY = $(window).scrollTop();

        if (ScrollY > NavY) {
            $('#menuContainer').addClass('sticky').removeClass('menu');
        } else {
            $('#menuContainer').removeClass('sticky').addClass('menu');

        }
    };

    $(window).scroll(function() {
        stickyNav();
    });


    //-------------------------------submit popup--------------------------------------

    var modal = document.getElementById('myModal');
    var btn = document.getElementById("submitBtn");

    btn.onclick = function() {
        modal.style.display = "block";
    };

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };

    //-------------------------------bootstrap slider--------------------------------------

    $('.carousel-control.left').click(function() {
        $('#myCarousel').carousel('prev');
    });

    $('.carousel-control.right').click(function() {
        $('#myCarousel').carousel('next');
    });

    //-------------------------------smooth scrolling--------------------------------------

    $("a").on('click', function(event) {

        if (this.hash !== "") {
            event.preventDefault();

            var hash = this.hash;

            $('html, body').animate({
                scrollTop: $(hash).offset().top-55
            }, 1000, function(){
                window.location.hash = hash;
            });
        }
    });

    var aChildren = $("nav li").children();
    var aArray = [];
    for (var i=0; i < aChildren.length; i++) {
        var aChild = aChildren[i];
        var ahref = $(aChild).attr('href');
        aArray.push(ahref);
    }

    //-------------------------------changing active menu item--------------------------------------

    $(window).scroll(function(){
        var windowPos = $(window).scrollTop();
        for (var i=0; i < aArray.length; i++) {
            var theID = aArray[i];
            var divPos = $(theID).offset().top-100;
            var divHeight = $(theID).height();
            if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                $("a[href='" + theID + "']").addClass("active");

            } else {
                $("a[href='" + theID + "']").removeClass("active");
            }
        }
    });

});


