

$(function() {
    $('#menubtn').on('click', function () {
        var x = $('section').css('margin-top');
        if (x == "57px")
            $('section').css({'margin-top': '200px'});
        else if (x == "200px")
            $('section').css({'margin-top': '57px'});
        else if (x == "108px")
            $('section').css({'margin-top': '250px'});
        else if (x == "250px")
            $('section').css({'margin-top': '108px'});
    });
    var counter = 0;

    $('#listBtn').on('click', function () {
        var list = document.getElementById('noteList');
        if (counter == 0) {
            document.getElementById('listBtn').innerHTML = "Add next element";
        }
        var field = '<input type="text" class="form-control" name="list[' + counter + ']" placeholder="' + counter + '"><br />';
        list.insertAdjacentHTML("beforeend", field);
        counter++;
        if (counter >= 10) {
            var btn = document.getElementById('listBtn');
            btn.innerHTML = "You cannot add more elements";
            btn.setAttribute('disabled', 'disabled');
            btn.setAttribute('class', 'btn btn-danger');
        }
    });

    (function changeToHyperlink() {
        try {
            var text = document.getElementById('notateTitle').innerHTML;
            var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            var text1 = text.replace(exp, "<a href='$1'>$1</a>");
            var exp2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
            document.getElementById("notateTitle").innerHTML = text1.replace(exp2, '$1<a target="_blank" href="http://$2">$2</a>');
            var text = document.getElementById('noteBody').innerHTML;
            var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
            var text1 = text.replace(exp, "<a href='$1'>$1</a>");
            var exp2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
            document.getElementById("noteBody").innerHTML = text1.replace(exp2, '$1<a target="_blank" href="http://$2">$2</a>');
        }
        catch (err){
            console.log("no note body class");
        }
    })();


});





