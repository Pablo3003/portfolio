<?php

namespace App\Http\Controllers;

use App\Note;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\DomCrawler\Form;

class NoteController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth');
        Carbon::setLocale('pl');
    }

    public function index(Request $request)
    {
        if($request['showarchived'])
            $notes = Note::orderBy('id', 'DESC')->where(['private' =>' false'])->get();
        else
            $notes = Note::orderBy('id', 'DESC')->where(['archived' => 'false', 'private' =>' false'])->get();
        return view('home', ['notes' => $notes, 'archived' => $request['showarchived'] ]);
    }


    public function createNote(Request $request)
    {
        $this->validate($request, [
            'body' => 'required|max:65535',
            'title' => 'required'
        ]);
        $note = new Note();
        $note->title = $request['title'];
        $note->body = $request['body'];
        $note->style = $request['style'];

        $note->alarm = sprintf('%s %s:00',$request['date'], $request['time']);
        if(!empty($request['list']))
            $note->list = serialize(array_filter($request['list']));
        $note->alarmString = $request['alarmString'];
        $note->private = ($request['private'] ? true : false);
        $request->user()->notes()->save($note);
        return redirect('/notes/id/'.$note->id)->with(['message' => 'Note created successfully']);

    }
    public function add()
    {
        return view('notes.new');
    }
    public function show($id)
    {
        $note=Note::findOrFail($id);
        if($note->private && $note->user_id != Auth::id())
            return redirect()->back();

        $note->list=unserialize($note->list);
        $result=0;
        preg_match(
            "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
            $note->body,$result);

        if($result)
        {
            $note->yt = '<iframe width="100%" height="350" src="https://www.youtube.com/embed/' .$result[2]. '" frameborder="0" allowfullscreen></iframe>';
        } //https://www.youtube.com/embed/3RdOylN0JFs?rel=0&autoplay=1
        return view('notes.show')->with(['note' => $note]);
    }
    public function archiveNote($id)
    {
        $note=Note::findOrFail($id);
        if(Auth::id() != $note->user_id)
            return reirect()->back();
        if($note->archived)
        {
            $note->archived = false;
            $msg="Note successfully restored";
        }
        else
        {
            $note->archived = true;
            $msg="Note successfully archived";
        }
        $note->save();
        return redirect()->back()->with(['message' => $msg]);
    }
    public function deleteNote($id)
    {
        $note=Note::findOrFail($id);
        if(Auth::id() != $note->user_id)
            return reirect()->back();
        $note->delete();
        return redirect('/home')->with(['message' => 'Note removed.']);
    }
    public function myNotes()
    {
        $notes = Note::orderBy('id', 'DESC')->where('user_id',Auth::id())->get();

        return view('notes.myNotes')->with(['notes' => $notes]);
    }
    public function shareNote($id)
    {
        $note=Note::findOrFail($id);
        if(Auth::id() != $note->user_id)
            return reirect()->back();

        $link= str_random(8);
        $note->share_link = $link;
        $note->save();
        return redirect()->back()->with(['message' => "Share link create."]);
    }
    public function sharedLink($token)
    {
        if(!$token)
            return view('welcome');
        $note = Note::where('share_link',$token)->first();

        if($note->private || $note->archived)
            return view('welcome')->withErrors(['error' => 'Note is private or archived']);

        return $this->show($note->id);

    }

}
