@extends('layouts.app')
@section('content')
<div class="section-content">
    <div class="row">
        @include('include.messages')
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Recent notes:
                <form method="post" action="{{ route('home') }}">
                    <label>Show archive:</label>
                    <input type="checkbox" value="showarchived" name="showarchived" onChange="this.form.submit()"
                    @if($archived) checked="checked" @endif>
                    <input type="hidden" value="{{ Session::token() }}" name="_token">
                </form>
                </div>
                <div class="modal-content">
                <div class="panel-body">
                    <ul class="list-group">
                    @foreach($notes as $note)
                        <li class="list-group-item">
                        <a href="{{ url('/notes/id/'.$note->id) }}">
                            <strong>{{$note['title']}}</strong>
                        </a>
                            @if($note->archived)
                                <span class="label label-default">Archived</span>
                            @endif
                            <p class="list-group-item-text">Autor: {{ $note->user->name }}, {{ $note->created_at->diffForHumans() }}</p>
                        </li>
                    @endforeach
                    </ul>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
