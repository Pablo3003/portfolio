@extends('layouts.app')

@section('content')
    <div class="section-content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    @include('include.messages')
                    <div class="panel-heading">Create your note!</div>
                        <div class="modal-content">
                            <form action="{{ route('note.create') }}" method="post">
                                <div class="panel-body">
                                    <input type="text" class="form-control" name=title" placeholder="Type the note title">
                                    <br/>
                                    <textarea class="form-control" rows="7" name="body" placeholder="Write a note in this area.  It's really easy to share with others. Click here ..."></textarea>
                                    <br/>
                                    <label for="private" style="color: black"> Private Note: </label>
                                    <input type="checkbox" value="private" name="private">
                                    <br/>
                                    <input type="hidden" value="{{ Session::token() }}" name="_token">
                                    <input type="text" class="form-control" class="input-group-addon" name="alarmString"  placeholder="Set a reminder for your note">
                                    <input type="text" id="date" name="date" class="form-control" placeholder="Begin date: YYYY-MM-DD">
                                    <input type="text" id="time" name="time" class="form-control" placeholder="Begin time: HH:MM">
                                    <br/>
                                    <span>List:</span>
                                    <div id="noteList">
                                    <!- TO MA ZOSTAĆ PUSTE -!>
                                    </div>
                                    <button type="button" class="btn btn-primary" id="listBtn"
                                        onclick="addList()">Add List
                                    </button>
                                    <br/>
                                    <br/>
                                    <label>Style:</label>
                                    <select name="style" class="form-control">
                                    <option value="noteLight">Jasny</option>
                                    <option value="noteDark">Ciemny</option>
                                    </select>
                                    <br/>
                                    <button type="submit" class="btn btn-primary">Add note</button>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
