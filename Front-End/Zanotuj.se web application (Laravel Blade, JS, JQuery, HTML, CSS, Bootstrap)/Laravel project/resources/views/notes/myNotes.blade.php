@extends('layouts.app')
@section('content')
    <div class="section-content">
        <div class="row">
            @include('include.messages')
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">My notes:</div>
                    <div class="modal-content">
                    <div class="panel-body">
                        <ul class="list-group">
                            @foreach($notes as $note)
                                <li class="list-group-item">
                                    <a href="{{ url('/notes/id/'.$note->id) }}">
                                        <strong>{{$note['title']}}</strong>
                                    </a>
                                    @if($note->archived)
                                        <span class="label label-default">Archived</span>
                                    @endif
                                    @if($note->private)
                                        <span class="label label-danger">Private</span>
                                    @endif
                                    <br />
                                    {{ $note->created_at->diffForHumans() }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection
