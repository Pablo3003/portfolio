@extends('layouts.app')

@section('content')
    <div class="section-content">
        <link href="/theme/css/{{$note->style}}.css" rel="stylesheet">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="noteContainer" >
                    <div class="panel panel-default">
                        @include('include.messages')
                        <div class="panel-heading" id="showNote">
                            <h4 class="noteTitle" id="notateTitle" >{{$note->title}}</h4>
                            <p class="text-muted noteOwner">{{$note->user->name}}
                                , {{ $note->created_at->diffForHumans() }}</p>
                            <div class="noteOptions">
                                @if($note->archived)
                                    <span class="label label-default">Archived</span>
                                @endif
                                @if($note->private)
                                    <span class="label label-danger">Private</span>
                                @endif

                                @if(Auth::id()==$note->user_id)
                                    <a href="{{ route('note.archive', ['id' => $note->id]) }}">
                                        @if($note->archived == false)
                                            Add to Archive
                                        @else
                                            Restore
                                        @endif

                                    </a> |
                                    <a href="{{ route('note.delete', ['id' => $note->id]) }}">Delete</a> |
                                    <a href="{{ route('note.share', ['id' => $note->id]) }}">Share</a>


                                    @if($note->share_link)
                                        <br>
                                        <label for="link">Share link: </label>
                                        <input type="text" contenteditable="true"
                                               value="{{Request::root().'/share/'.$note->share_link}}"
                                               readonly="readonly">
                                    @endif

                                    @if($note->alarmString)
                                        <br /><br />
                                        <label>Reminder:</label>
                                        <p>{{$note->alarmString}}</p>
                                        <p class="text-muted">{{$note->alarm}}</p>
                                    @endif
                                @endif

                            </div>

                        </div>

                        <div class="panel-body noteContent">
                            <article id="noteBody">
                                {!! nl2br(e($note->body)) !!}

                            </article>

                            <div class="noteTY">
                                <p id="youtube">{!!$note->yt  !!}</p>
                            </div>
                            <br>
                            @if($note->list)
                                <div class="noteList">
                                    <label>Lista:</label>
                                    <ul class="list-group">
                                        @foreach($note->list as $listItem)
                                            <li class="list-group-item">{{$listItem}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
