
(function() {

    //----------------------------sticky navigation--------------------------------------------
    var NavY = $('#menu').addClass('menu').offset().top;

    var stickyNav = function () {
        var ScrollY = $(window).scrollTop();

        if (ScrollY > NavY) {
            $('#menu').addClass('sticky').removeClass('menu');
        } else {
            $('#menu').removeClass('sticky').addClass('menu');

        }
    };

    $(window).scroll(function () {
        stickyNav();
    });


    //----------------------------game_flow window--------------------------------------------


    function minimizeGameWindow() {
        for (var i = 1; i < 4; i++) {
            var column_name = "#column" + i;
            console.log(column_name);
            $(column_name).removeClass("col-md-4 col-sm-3").addClass("col-md-2 col-sm-2");
            $('#column1').addClass('col-md-offset-2 col-sm-offset-3');
            $('#game_table').css({"right": "400px", "top": "30vh"});
        }
        return false;
    }

    function scroll_to(selector) {
        $('html,body').animate({scrollTop: $(selector).offset().top - 150}, 1000);
        minimizeGameWindow();
    }

    var check_tail_table = [0, 1, 2, 3, 4, 5, 6, 7, 8];

    function check_tail(event) {
        click_check(check_tail_table[event.data.param1]);
        click_number++;
    }

    var combination = [];

    function game_init() {
        for (var i = 0; i < 99; i++) {
            combination.push(Math.floor(Math.random() * 9) + 1);
        }
        for (var j = 0; j < 9; j++) {
            $("#tail" + (j + 1)).on('click', {param1: j}, check_tail)
        }
    }

    function game_block() {
        for (var j = 0; j < 9; j++) {
            $("#tail" + (j + 1)).css("pointer-events", "none");
        }
    }

    function game_unblock() {
        for (var j = 0; j < 9; j++) {
            $("#tail" + (j + 1)).css("pointer-events", "auto");
        }
    }

    function game_start(val) {
        game_block();
        var popup = document.getElementById('myModal');
        if (val === false)
            popup.classList.add("modal--show");
    }

    function modal_init() {
        $('#close').on('click', function () {
            modal.classList.remove("modal--show");
            modal.classList.add("modal--hide");
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.classList.add("modal--hide");
                }
            };
            game_start(true);
            game_block();
            game_flow();
            round++;
        });
    }

    if (top.location.pathname.match('/mozg'))
    {
    	scroll_to('.content');
    	game_init();
    	game_start();

        var modal = document.getElementById('myModal');
        modal.classList.add("modal--show");

        modal_init();
    }

    function game_flow() {

        document.getElementById("box1").innerText = "Zapamietaj Sekwencje";
        document.getElementById("box2").innerText = "Runda: " + (round + 2);

        var single_time_spacing = 1000;
        var whole_time = 1000;

        var number = round + 2;

        function tail_mark(combination) {
            whole_time = single_time_spacing + 1000;
            var tail = $("#tail" + combination[j]);
            setTimeout(function () {
                tail.addClass("tail_game")
            }, single_time_spacing);
            setTimeout(function () {
                tail.removeClass("tail_game")
            }, whole_time);
            single_time_spacing = single_time_spacing + 2000;
        }

        for (var j = 0; j < number; j++) {
            tail_mark(combination);
        }

        setTimeout(function () {
            document.getElementById("box1").innerText = "Powtorz Sekwencje"
            game_unblock();
        }, whole_time);
    }

    var good_snd = new Audio("../cube.mp3");
    var fail_snd = new Audio("../fail.mp3");
    var click_number = 0;
    var round = -1;

    function click_check(check) {
        check++;
        if (check !== combination[click_number]) {
            modal.innerHTML ='<div class="modal__content"><span class="close" id="close">&times;</span><p class= "modal__content__text">Zła odpowiedź, zamknij aby rozpocząć od nowa</p> </span> </div>';
            var content = modal.querySelector('.modal__content');
            content.style.width = "30%";
            content.style.height = "30%";
            modal_init();
            fail_snd.play();
            check--;
            round = -1;
            click_number = -1;
            game_start(false);


        }
        else {
            good_snd.play();
            modal.innerHTML ='<div class="modal__content"><span class="close" id="close">&times;</span><p class= "modal__content__text">Gratulacje zdobywasz punkt, zamknij aby kontynuowac</p> </span> </div>';
            var content = modal.querySelector('.modal__content');
            content.style.width = "30%";
            content.style.height = "30%";
            modal_init();
            check--;
            round_change();
        }
    }

    function round_change() {

        if (round == click_number) {
            click_number = -1;
            game_start(false);
        }
    }


	/*----------------------------------------------notacja----------------------------------------*/

    (function button_init() {
        for (var j = 1; j < 13; j++) {
            $("#button" + j).on('click', {param1: j}, button)
        }
    }());


    function button(event) {
        var el = document.getElementById('parent');
        switch (event.data.param1) {
            case 1: {
                el.innerHTML = '<div id="layer"><div class="wall wall1" id="wall1_4"></div><div class="wall wall1" id="wall1_5"></div><div class="wall wall1" id="wall1_6"></div><div class="wall wall2" id="wall2_4"></div> <div class="wall wall2" id="wall2_5"></div><div class="wall wall2" id="wall2_6"></div><div class="wall wall3" id="wall3_4"></div><div class="wall wall3" id="wall3_5"></div><div class="wall wall3" id="wall3_6"></div><div class="wall wall4" id="wall4_4"></div><div class="wall wall4" id="wall4_5"></div><div class="wall wall4" id="wall4_6"></div><div class="wall wall5" id="wall5_1"></div><div class="wall wall5" id="wall5_2"></div><div class="wall wall5" id="wall5_3"></div><div class="wall wall5" id="wall5_4"></div><div class="wall wall5" id="wall5_5"></div><div class="wall wall5" id="wall5_6"></div><div class="wall wall5" id="wall5_7"></div><div class="wall wall5" id="wall5_8"></div><div class="wall wall5" id="wall5_9"></div></div><div class="wall wall1" id="wall1_1"></div><div class="wall wall1" id="wall1_2"></div><div class="wall wall1" id="wall1_3"></div><div class="wall wall1" id="wall1_7"></div><div class="wall wall1" id="wall1_8"></div><div class="wall wall1" id="wall1_9"></div><div class="wall wall2" id="wall2_1"></div><div class="wall wall2" id="wall2_2"></div><div class="wall wall2" id="wall2_3"></div><div class="wall wall2" id="wall2_7"></div><div class="wall wall2" id="wall2_8"></div><div class="wall wall2" id="wall2_9"></div><div class="wall wall3" id="wall3_1"></div><div class="wall wall3" id="wall3_2"></div><div class="wall wall3" id="wall3_3"></div><div class="wall wall3" id="wall3_7"></div><div class="wall wall3" id="wall3_8"></div><div class="wall wall3" id="wall3_9"></div><div class="wall wall4" id="wall4_1"></div><div class="wall wall4" id="wall4_2"></div><div class="wall wall4" id="wall4_3"></div><div class="wall wall4" id="wall4_7"></div><div class="wall wall4" id="wall4_8"></div><div class="wall wall4" id="wall4_9"></div><div class="wall wall6" id="wall6_1"></div><div class="wall wall6" id="wall6_2"></div><div class="wall wall6" id="wall6_3"></div><div class="wall wall6" id="wall6_4"></div><div class="wall wall6" id="wall6_5"></div><div class="wall wall6" id="wall6_6"></div><div class="wall wall6" id="wall6_7"></div><div class="wall wall6" id="wall6_8"></div><div class="wall wall6" id="wall6_9"></div></div>';
                var el2 = document.getElementById('layer');
                window.setTimeout(function () {
                    el2.style.animationName = "obrotU";
                }, 1);
            }
                break;

            case 2: {
                el.innerHTML = '<div id="layer"><div class="wall wall1" id="wall1_3"></div><div class="wall wall1" id="wall1_6"></div><div class="wall wall1" id="wall1_9"></div><div class="wall wall2" id="wall2_3"></div> <div class="wall wall2" id="wall2_6"></div><div class="wall wall2" id="wall2_9"></div><div class="wall wall5" id="wall5_3"></div><div class="wall wall5" id="wall5_6"></div><div class="wall wall5" id="wall5_9"></div><div class="wall wall6" id="wall6_3"></div><div class="wall wall6" id="wall6_6"></div><div class="wall wall6" id="wall6_9"></div><div class="wall wall4" id="wall4_1"></div><div class="wall wall4" id="wall4_2"></div><div class="wall wall4" id="wall4_3"></div><div class="wall wall4" id="wall4_4"></div><div class="wall wall4" id="wall4_5"></div><div class="wall wall4" id="wall4_6"></div><div class="wall wall4" id="wall4_7"></div><div class="wall wall4" id="wall4_8"></div><div class="wall wall4" id="wall4_9"></div></div><div class="wall wall1" id="wall1_1"></div><div class="wall wall1" id="wall1_2"></div><div class="wall wall1" id="wall1_4"></div><div class="wall wall1" id="wall1_5"></div><div class="wall wall1" id="wall1_7"></div><div class="wall wall1" id="wall1_8"></div><div class="wall wall2" id="wall2_1"></div><div class="wall wall2" id="wall2_2"></div><div class="wall wall2" id="wall2_4"></div><div class="wall wall2" id="wall2_5"></div><div class="wall wall2" id="wall2_7"></div><div class="wall wall2" id="wall2_8"></div><div class="wall wall3" id="wall3_1"></div><div class="wall wall3" id="wall3_2"></div><div class="wall wall3" id="wall3_3"></div><div class="wall wall3" id="wall3_4"></div><div class="wall wall3" id="wall3_5"></div><div class="wall wall3" id="wall3_6"></div><div class="wall wall3" id="wall3_7"></div><div class="wall wall3" id="wall3_8"></div><div class="wall wall3" id="wall3_9"></div><div class="wall wall5" id="wall5_1"></div><div class="wall wall5" id="wall5_2"></div><div class="wall wall5" id="wall5_4"></div><div class="wall wall5" id="wall5_5"></div><div class="wall wall5" id="wall5_7"></div><div class="wall wall5" id="wall5_8"></div><div class="wall wall6" id="wall6_1"></div><div class="wall wall6" id="wall6_2"></div><div class="wall wall6" id="wall6_4"></div><div class="wall wall6" id="wall6_5"></div><div class="wall wall6" id="wall6_7"></div><div class="wall wall6" id="wall6_8"></div>';
                var el2 = document.getElementById('layer');
                window.setTimeout(function () {
                    el2.style.animationName = "obrotL";
                }, 1);
            }
                break;

            case 3: {
                el.innerHTML = '<div id="layer"><div class="wall wall1" id="wall1_1"></div><div class="wall wall1" id="wall1_2"></div><div class="wall wall1" id="wall1_3"></div><div class="wall wall1" id="wall1_4"></div><div class="wall wall1" id="wall1_5"></div><div class="wall wall1"id="wall1_6"></div><div class="wall wall1" id="wall1_7"></div><div class="wall wall1" id="wall1_8"></div><div class="wall wall1" id="wall1_9"></div><div class="wall wall3" id="wall3_3"></div><div class="wall wall3" id="wall3_6"></div><div class="wall wall3" id="wall3_9"></div><div class="wall wall4" id="wall4_8"></div><div class="wall wall4" id="wall4_5"></div><div class="wall wall4" id="wall4_2"></div><div class="wall wall5" id="wall5_7"></div><div class="wall wall5" id="wall5_8"></div><div class="wall wall5" id="wall5_9"></div><div class="wall wall6" id="wall6_4"></div><div class="wall wall6" id="wall6_5"></div><div class="wall wall6" id="wall6_6"></div></div><div class="wall wall2" id="wall2_1"></div><div class="wall wall2" id="wall2_2"></div><div class="wall wall2" id="wall2_3"></div><div class="wall wall2" id="wall2_4"></div><div class="wall wall2" id="wall2_5"></div><div class="wall wall2" id="wall2_6"></div><div class="wall wall2" id="wall2_7"></div><div class="wall wall2" id="wall2_8"></div><div class="wall wall2" id="wall2_9"></div><div class="wall wall3" id="wall3_1"></div><div class="wall wall3" id="wall3_2"></div><div class="wall wall3" id="wall3_4"></div><div class="wall wall3" id="wall3_5"></div><div class="wall wall3" id="wall3_7"></div><div class="wall wall3" id="wall3_8"></div><div class="wall wall4" id="wall4_1"></div><div class="wall wall4" id="wall4_3"></div><div class="wall wall4" id="wall4_4"></div><div class="wall wall4" id="wall4_6"></div><div class="wall wall4" id="wall4_7"></div><div class="wall wall4" id="wall4_9"></div><div class="wall wall5" id="wall5_1"></div><div class="wall wall5" id="wall5_2"></div><div class="wall wall5" id="wall5_3"></div><div class="wall wall5" id="wall5_4"></div><div class="wall wall5" id="wall5_5"></div><div class="wall wall5" id="wall5_6"></div><div class="wall wall6" id="wall6_1"></div><div class="wall wall6" id="wall6_2"></div><div class="wall wall6" id="wall6_3"></div><div class="wall wall6" id="wall6_7"></div><div class="wall wall6" id="wall6_8"></div><div class="wall wall6" id="wall6_9"></div>';

                var el2 = document.getElementById('layer');
                window.setTimeout(function () {
                    el2.style.animationName = "obrotF";
                }, 1);
            }
                break;

            case 4: {
                el.innerHTML = '<div id="layer"><div class="wall wall3" id="wall3_1"></div><div class="wall wall3" id="wall3_2"></div><div class="wall wall3" id="wall3_3"></div><div class="wall wall3" id="wall3_4"></div><div class="wall wall3" id="wall3_5"></div><div class="wall wall3" id="wall3_6"></div><div class="wall wall3" id="wall3_7"></div><div class="wall wall3" id="wall3_8"></div><div class="wall wall3" id="wall3_9"></div><div class="wall wall1" id="wall1_2"></div><div class="wall wall1" id="wall1_5"></div><div class="wall wall1" id="wall1_8"></div><div class="wall wall2" id="wall2_2"></div><div class="wall wall2" id="wall2_5"></div><div class="wall wall2" id="wall2_8"></div><div class="wall wall5" id="wall5_2"></div><div class="wall wall5" id="wall5_5"></div><div class="wall wall5" id="wall5_8"></div><div class="wall wall6" id="wall6_2"></div><div class="wall wall6" id="wall6_5"></div><div class="wall wall6" id="wall6_8"></div></div><div class="wall wall1" id="wall1_1"></div><div class="wall wall1" id="wall1_3"></div><div class="wall wall1" id="wall1_4"></div><div class="wall wall1" id="wall1_6"></div><div class="wall wall1" id="wall1_7"></div><div class="wall wall1" id="wall1_9"></div><div class="wall wall2" id="wall2_1"></div><div class="wall wall2" id="wall2_3"></div><div class="wall wall2" id="wall2_4"></div><div class="wall wall2" id="wall2_6"></div><div class="wall wall2" id="wall2_7"></div><div class="wall wall2" id="wall2_9"></div><div class="wall wall4" id="wall4_1"></div><div class="wall wall4" id="wall4_2"></div><div class="wall wall4" id="wall4_3"></div><div class="wall wall4" id="wall4_4"></div><div class="wall wall4" id="wall4_5"></div><div class="wall wall4" id="wall4_6"></div><div class="wall wall4" id="wall4_7"></div><div class="wall wall4" id="wall4_8"></div><div class="wall wall4" id="wall4_9"></div><div class="wall wall5" id="wall5_1"></div><div class="wall wall5" id="wall5_3"></div><div class="wall wall5" id="wall5_4"></div><div class="wall wall5" id="wall5_6"></div><div class="wall wall5" id="wall5_7"></div><div class="wall wall5" id="wall5_9"></div><div class="wall wall6" id="wall6_1"></div><div class="wall wall6" id="wall6_3"></div><div class="wall wall6" id="wall6_4"></div><div class="wall wall6" id="wall6_6"></div><div class="wall wall6" id="wall6_7"></div><div class="wall wall6" id="wall6_9"></div>';

                var el2 = document.getElementById('layer');
                window.setTimeout(function () {
                    el2.style.animationName = "obrotR";
                }, 1);
            }
                break;

            case 5: {
                el.innerHTML = '<div id="layer"><div class="wall wall2" id="wall2_1"></div><div class="wall wall2" id="wall2_2"></div><div class="wall wall2" id="wall2_3"></div><div class="wall wall2" id="wall2_4"></div><div class="wall wall2" id="wall2_5"></div><div class="wall wall2" id="wall2_6"></div><div class="wall wall2" id="wall2_7"></div><div class="wall wall2" id="wall2_8"></div><div class="wall wall2" id="wall2_9"></div><div class="wall wall3" id="wall3_2"></div><div class="wall wall3" id="wall3_5"></div><div class="wall wall3" id="wall3_8"></div><div class="wall wall4" id="wall4_3"></div><div class="wall wall4" id="wall4_6"></div><div class="wall wall4" id="wall4_9"></div><div class="wall wall5" id="wall5_4"></div><div class="wall wall5" id="wall5_5"></div><div class="wall wall5" id="wall5_6"></div><div class="wall wall6" id="wall6_7"></div><div class="wall wall6" id="wall6_8"></div><div class="wall wall6" id="wall6_9"></div></div><div class="wall wall1" id="wall1_1"></div><div class="wall wall1" id="wall1_2"></div><div class="wall wall1" id="wall1_3"></div><div class="wall wall1" id="wall1_4"></div><div class="wall wall1" id="wall1_5"></div><div class="wall wall1" id="wall1_6"></div><div class="wall wall1" id="wall1_7"></div><div class="wall wall1" id="wall1_8"></div><div class="wall wall1" id="wall1_9"></div><div class="wall wall3" id="wall3_1"></div><div class="wall wall3" id="wall3_3"></div><div class="wall wall3" id="wall3_4"></div><div class="wall wall3" id="wall3_6"></div><div class="wall wall3" id="wall3_7"></div><div class="wall wall3" id="wall3_9"></div><div class="wall wall4" id="wall4_1"></div><div class="wall wall4" id="wall4_2"></div><div class="wall wall4" id="wall4_4"></div><div class="wall wall4" id="wall4_5"></div><div class="wall wall4" id="wall4_7"></div><div class="wall wall4" id="wall4_8"></div><div class="wall wall5" id="wall5_1"></div><div class="wall wall5" id="wall5_2"></div><div class="wall wall5" id="wall5_3"></div><div class="wall wall5" id="wall5_7"></div><div class="wall wall5" id="wall5_8"></div><div class="wall wall5" id="wall5_9"></div><div class="wall wall6" id="wall6_1"></div><div class="wall wall6" id="wall6_2"></div><div class="wall wall6" id="wall6_3"></div><div class="wall wall6" id="wall6_4"></div><div class="wall wall6" id="wall6_5"></div><div class="wall wall6" id="wall6_6"></div>';
                var el2 = document.getElementById('layer');
                window.setTimeout(function () {
                    el2.style.animationName = "obrotB";
                }, 1);
            }
                break;

            case 6: {
                el.innerHTML = '<div id="layer"><div class="wall wall6" id="wall6_1"></div><div class="wall wall6" id="wall6_2"></div><div class="wall wall6" id="wall6_3"></div><div class="wall wall6" id="wall6_4"></div><div class="wall wall6" id="wall6_5"></div><div class="wall wall6" id="wall6_6"></div><div class="wall wall6" id="wall6_7"></div><div class="wall wall6" id="wall6_8"></div><div class="wall wall6" id="wall6_9"></div><div class="wall wall1" id="wall1_7"></div><div class="wall wall1" id="wall1_8"></div><div class="wall wall1" id="wall1_9"></div><div class="wall wall2" id="wall2_7"></div><div class="wall wall2" id="wall2_8"></div><div class="wall wall2" id="wall2_9"></div><div class="wall wall3" id="wall3_7"></div><div class="wall wall3" id="wall3_8"></div><div class="wall wall3" id="wall3_9"></div><div class="wall wall4" id="wall4_7"></div><div class="wall wall4" id="wall4_8"></div><div class="wall wall4" id="wall4_9"></div></div><div class="wall wall1" id="wall1_1"></div><div class="wall wall1" id="wall1_2"></div><div class="wall wall1" id="wall1_3"></div><div class="wall wall1" id="wall1_4"></div><div class="wall wall1" id="wall1_5"></div><div class="wall wall1" id="wall1_6"></div><div class="wall wall2" id="wall2_1"></div><div class="wall wall2" id="wall2_2"></div><div class="wall wall2" id="wall2_3"></div><div class="wall wall2" id="wall2_4"></div><div class="wall wall2" id="wall2_5"></div><div class="wall wall2" id="wall2_6"></div><div class="wall wall3" id="wall3_1"></div><div class="wall wall3" id="wall3_2"></div><div class="wall wall3" id="wall3_3"></div><div class="wall wall3" id="wall3_4"></div><div class="wall wall3" id="wall3_5"></div><div class="wall wall3" id="wall3_6"></div><div class="wall wall4" id="wall4_1"></div><div class="wall wall4" id="wall4_2"></div><div class="wall wall4" id="wall4_3"></div><div class="wall wall4" id="wall4_4"></div><div class="wall wall4" id="wall4_5"></div><div class="wall wall4" id="wall4_6"></div><div class="wall wall5" id="wall5_1"></div><div class="wall wall5" id="wall5_2"></div><div class="wall wall5" id="wall5_3"></div><div class="wall wall5" id="wall5_4"></div><div class="wall wall5" id="wall5_5"></div><div class="wall wall5" id="wall5_6"></div><div class="wall wall5" id="wall5_7"></div><div class="wall wall5" id="wall5_8"></div><div class="wall wall5" id="wall5_9"></div>';

                var el2 = document.getElementById('layer');
                window.setTimeout(function () {
                    el2.style.animationName = "obrotD";
                }, 1);
            }
                break;

            case 7: {
                el.innerHTML = '<div id="layer"><div class="wall wall1" id="wall1_4"></div><div class="wall wall1" id="wall1_5"></div><div class="wall wall1" id="wall1_6"></div><div class="wall wall2" id="wall2_4"></div> <div class="wall wall2" id="wall2_5"></div><div class="wall wall2" id="wall2_6"></div><div class="wall wall3" id="wall3_4"></div><div class="wall wall3" id="wall3_5"></div><div class="wall wall3" id="wall3_6"></div><div class="wall wall4" id="wall4_4"></div><div class="wall wall4" id="wall4_5"></div><div class="wall wall4" id="wall4_6"></div><div class="wall wall5" id="wall5_1"></div><div class="wall wall5" id="wall5_2"></div><div class="wall wall5" id="wall5_3"></div><div class="wall wall5" id="wall5_4"></div><div class="wall wall5" id="wall5_5"></div><div class="wall wall5" id="wall5_6"></div><div class="wall wall5" id="wall5_7"></div><div class="wall wall5" id="wall5_8"></div><div class="wall wall5" id="wall5_9"></div></div><div class="wall wall1" id="wall1_1"></div><div class="wall wall1" id="wall1_2"></div><div class="wall wall1" id="wall1_3"></div><div class="wall wall1" id="wall1_7"></div><div class="wall wall1" id="wall1_8"></div><div class="wall wall1" id="wall1_9"></div><div class="wall wall2" id="wall2_1"></div><div class="wall wall2" id="wall2_2"></div><div class="wall wall2" id="wall2_3"></div><div class="wall wall2" id="wall2_7"></div><div class="wall wall2" id="wall2_8"></div><div class="wall wall2" id="wall2_9"></div><div class="wall wall3" id="wall3_1"></div><div class="wall wall3" id="wall3_2"></div><div class="wall wall3" id="wall3_3"></div><div class="wall wall3" id="wall3_7"></div><div class="wall wall3" id="wall3_8"></div><div class="wall wall3" id="wall3_9"></div><div class="wall wall4" id="wall4_1"></div><div class="wall wall4" id="wall4_2"></div><div class="wall wall4" id="wall4_3"></div><div class="wall wall4" id="wall4_7"></div><div class="wall wall4" id="wall4_8"></div><div class="wall wall4" id="wall4_9"></div><div class="wall wall6" id="wall6_1"></div><div class="wall wall6" id="wall6_2"></div><div class="wall wall6" id="wall6_3"></div><div class="wall wall6" id="wall6_4"></div><div class="wall wall6" id="wall6_5"></div><div class="wall wall6" id="wall6_6"></div><div class="wall wall6" id="wall6_7"></div><div class="wall wall6" id="wall6_8"></div><div class="wall wall6" id="wall6_9"></div></div>';
                var el2 = document.getElementById('layer');
                window.setTimeout(function () {
                    el2.style.animationName = "obrotUp";
                }, 1);
            }
                break;

            case 8: {
                el.innerHTML = '<div id="layer"><div class="wall wall1" id="wall1_3"></div><div class="wall wall1" id="wall1_6"></div><div class="wall wall1" id="wall1_9"></div><div class="wall wall2" id="wall2_3"></div> <div class="wall wall2" id="wall2_6"></div><div class="wall wall2" id="wall2_9"></div><div class="wall wall5" id="wall5_3"></div><div class="wall wall5" id="wall5_6"></div><div class="wall wall5" id="wall5_9"></div><div class="wall wall6" id="wall6_3"></div><div class="wall wall6" id="wall6_6"></div><div class="wall wall6" id="wall6_9"></div><div class="wall wall4" id="wall4_1"></div><div class="wall wall4" id="wall4_2"></div><div class="wall wall4" id="wall4_3"></div><div class="wall wall4" id="wall4_4"></div><div class="wall wall4" id="wall4_5"></div><div class="wall wall4" id="wall4_6"></div><div class="wall wall4" id="wall4_7"></div><div class="wall wall4" id="wall4_8"></div><div class="wall wall4" id="wall4_9"></div></div><div class="wall wall1" id="wall1_1"></div><div class="wall wall1" id="wall1_2"></div><div class="wall wall1" id="wall1_4"></div><div class="wall wall1" id="wall1_5"></div><div class="wall wall1" id="wall1_7"></div><div class="wall wall1" id="wall1_8"></div><div class="wall wall2" id="wall2_1"></div><div class="wall wall2" id="wall2_2"></div><div class="wall wall2" id="wall2_4"></div><div class="wall wall2" id="wall2_5"></div><div class="wall wall2" id="wall2_7"></div><div class="wall wall2" id="wall2_8"></div><div class="wall wall3" id="wall3_1"></div><div class="wall wall3" id="wall3_2"></div><div class="wall wall3" id="wall3_3"></div><div class="wall wall3" id="wall3_4"></div><div class="wall wall3" id="wall3_5"></div><div class="wall wall3" id="wall3_6"></div><div class="wall wall3" id="wall3_7"></div><div class="wall wall3" id="wall3_8"></div><div class="wall wall3" id="wall3_9"></div><div class="wall wall5" id="wall5_1"></div><div class="wall wall5" id="wall5_2"></div><div class="wall wall5" id="wall5_4"></div><div class="wall wall5" id="wall5_5"></div><div class="wall wall5" id="wall5_7"></div><div class="wall wall5" id="wall5_8"></div><div class="wall wall6" id="wall6_1"></div><div class="wall wall6" id="wall6_2"></div><div class="wall wall6" id="wall6_4"></div><div class="wall wall6" id="wall6_5"></div><div class="wall wall6" id="wall6_7"></div><div class="wall wall6" id="wall6_8"></div>';
                var el2 = document.getElementById('layer');
                window.setTimeout(function () {
                    el2.style.animationName = "obrotLp";
                }, 1);
            }
                break;

            case 9: {
                el.innerHTML = '<div id="layer"><div class="wall wall1" id="wall1_1"></div><div class="wall wall1" id="wall1_2"></div><div class="wall wall1" id="wall1_3"></div><div class="wall wall1" id="wall1_4"></div><div class="wall wall1" id="wall1_5"></div><div class="wall wall1"id="wall1_6"></div><div class="wall wall1" id="wall1_7"></div><div class="wall wall1" id="wall1_8"></div><div class="wall wall1" id="wall1_9"></div><div class="wall wall3" id="wall3_3"></div><div class="wall wall3" id="wall3_6"></div><div class="wall wall3" id="wall3_9"></div><div class="wall wall4" id="wall4_8"></div><div class="wall wall4" id="wall4_5"></div><div class="wall wall wall4" id="wall4_2"></div><div class="wall wall5" id="wall5_7"></div><div class="wall wall5" id="wall5_8"></div><div class="wall wall5" id="wall5_9"></div><div class="wall wall6" id="wall6_4"></div><div class="wall wall6" id="wall6_5"></div><div class="wall wall6" id="wall6_6"></div></div><div class="wall wall2" id="wall2_1"></div><div class="wall wall2" id="wall2_2"></div><div class="wall wall2" id="wall2_3"></div><div class="wall wall2" id="wall2_4"></div><div class="wall wall2" id="wall2_5"></div><div class="wall wall2" id="wall2_6"></div><div class="wall wall2" id="wall2_7"></div><div class="wall wall2" id="wall2_8"></div><div class="wall wall2" id="wall2_9"></div><div class="wall wall3" id="wall3_1"></div><div class="wall wall3" id="wall3_2"></div><div class="wall wall3" id="wall3_4"></div><div class="wall wall3" id="wall3_5"></div><div class="wall wall3" id="wall3_7"></div><div class="wall wall3" id="wall3_8"></div><div class="wall wall4" id="wall4_1"></div><div class="wall wall4" id="wall4_3"></div><div class="wall wall4" id="wall4_4"></div><div class="wall wall4" id="wall4_6"></div><div class="wall wall4" id="wall4_7"></div><div class="wall wall4" id="wall4_9"></div><div class="wall wall5" id="wall5_1"></div><div class="wall wall5" id="wall5_2"></div><div class="wall wall5" id="wall5_3"></div><div class="wall wall5" id="wall5_4"></div><div class="wall wall5" id="wall5_5"></div><div class="wall wall5" id="wall5_6"></div><div class="wall wall6" id="wall6_1"></div><div class="wall wall6" id="wall6_2"></div><div class="wall wall6" id="wall6_3"></div><div class="wall wall6" id="wall6_7"></div><div class="wall wall6" id="wall6_8"></div><div class="wall wall6" id="wall6_9"></div>';

                var el2 = document.getElementById('layer');
                window.setTimeout(function () {
                    el2.style.animationName = "obrotFp";
                }, 1);
            }
                break;

            case 10: {
                el.innerHTML = '<div id="layer"><div class="wall wall3" id="wall3_1"></div><div class="wall wall3" id="wall3_2"></div><div class="wall wall3" id="wall3_3"></div><div class="wall wall3" id="wall3_4"></div><div class="wall wall3" id="wall3_5"></div><div class="wall wall3" id="wall3_6"></div><div class="wall wall3" id="wall3_7"></div><div class="wall wall3" id="wall wall3_8"></div><div class="wall wall3" id="wall3_9"></div><div class="wall wall1" id="wall1_2"></div><div class="wall wall1" id="wall1_5"></div><div class="wall wall1" id="wall1_8"></div><div class="wall wall2" id="wall2_2"></div><div class="wall wall2" id="wall2_5"></div><div class="wall wall2" id="wall2_8"></div><div class="wall wall5" id="wall5_2"></div><div class="wall wall5" id="wall5_5"></div><div class="wall wall5" id="wall5_8"></div><div class="wall wall6" id="wall6_2"></div><div class="wall wall6" id="wall6_5"></div><div class="wall wall6" id="wall6_8"></div></div><div class="wall wall1" id="wall1_1"></div><div class="wall wall1" id="wall1_3"></div><div class="wall wall1" id="wall1_4"></div><div class="wall wall1" id="wall1_6"></div><div class="wall wall1" id="wall1_7"></div><div class="wall wall1" id="wall1_9"></div><div class="wall wall2" id="wall2_1"></div><div class="wall wall2" id="wall2_3"></div><div class="wall wall2" id="wall2_4"></div><div class="wall wall2" id="wall2_6"></div><div class="wall wall2" id="wall2_7"></div><div class="wall wall2" id="wall2_9"></div><div class="wall wall4" id="wall4_1"></div><div class="wall wall4" id="wall4_2"></div><div class="wall wall4" id="wall4_3"></div><div class="wall wall4" id="wall4_4"></div><div class="wall wall4" id="wall4_5"></div><div class="wall wall4" id="wall4_6"></div><div class="wall wall4" id="wall4_7"></div><div class="wall wall4" id="wall4_8"></div><div class="wall wall4" id="wall4_9"></div><div class="wall wall5" id="wall5_1"></div><div class="wall wall5" id="wall5_3"></div><div class="wall wall5" id="wall5_4"></div><div class="wall wall5" id="wall5_6"></div><div class="wall wall5" id="wall5_7"></div><div class="wall wall5" id="wall5_9"></div><div class="wall wall6" id="wall6_1"></div><div class="wall wall6" id="wall6_3"></div><div class="wall wall6" id="wall6_4"></div><div class="wall wall6" id="wall6_6"></div><div class="wall wall6" id="wall6_7"></div><div class="wall wall6" id="wall6_9"></div>';

                var el2 = document.getElementById('layer');
                window.setTimeout(function () {
                    el2.style.animationName = "obrotRp";
                }, 1);
            }
                break;

            case 11: {
                el.innerHTML = '<div id="layer"><div class="wall wall2" id="wall2_1"></div><div class="wall wall2" id="wall2_2"></div><div class="wall wall2" id="wall2_3"></div><div class="wall wall2" id="wall2_4"></div><div class="wall wall2" id="wall2_5"></div><div class="wall wall2" id="wall2_6"></div><div class="wall wall2" id="wall2_7"></div><div class="wall wall2" id="wall2_8"></div><div class="wall wall2" id="wall2_9"></div><div class=" wall wall3" id="wall3_2"></div><div class="wall wall3" id="wall3_5"></div><div class=" wall wall3" id="wall3_8"></div><div class="wall wall4" id="wall4_3"></div><div class="wall wall4" id="wall4_6"></div><div class="wall wall4" id="wall4_9"></div><div class="wall wall5" id="wall5_4"></div><div class="wall wall5" id="wall5_5"></div><div class="wall wall5" id="wall5_6"></div><div class="wall wall6" id="wall6_7"></div><div class="wall wall6" id="wall6_8"></div><div class="wall wall6" id="wall6_9"></div></div><div class="wall wall1" id="wall1_1"></div><div class="wall wall1" id="wall1_2"></div><div class="wall wall1" id="wall1_3"></div><div class="wall wall1" id="wall1_4"></div><div class="wall wall1" id="wall1_5"></div><div class="wall wall1" id="wall1_6"></div><div class="wall wall1" id="wall1_7"></div><div class="wall wall1" id="wall1_8"></div><div class="wall wall1" id="wall1_9"></div><div class="wall wall3" id="wall3_1"></div><div class="wall wall3" id="wall3_3"></div><div class="wall wall3" id="wall3_4"></div><div class="wall wall3" id="wall3_6"></div><div class="wall wall3" id="wall3_7"></div><div class="wall wall3" id="wall3_9"></div><div class="wall wall4" id="wall4_1"></div><div class="wall wall4" id="wall4_2"></div><div class="wall wall4" id="wall4_4"></div><div class="wall wall4" id="wall4_5"></div><div class="wall wall4" id="wall4_7"></div><div class="wall wall4" id="wall4_8"></div><div class="wall wall5" id="wall5_1"></div><div class="wall wall5" id="wall5_2"></div><div class="wall wall5" id="wall5_3"></div><div class="wall wall5" id="wall5_7"></div><div class="wall wall5" id="wall5_8"></div><div class="wall wall5" id="wall5_9"></div><div class="wall wall6" id="wall6_1"></div><div class="wall wall6" id="wall6_2"></div><div class="wall wall6" id="wall6_3"></div><div class="wall wall6" id="wall6_4"></div><div class="wall wall6" id="wall6_5"></div><div class="wall wall6" id="wall6_6"></div>';
                var el2 = document.getElementById('layer');
                window.setTimeout(function () {
                    el2.style.animationName = "obrotBp";
                }, 1);
            }
                break;

            case 12: {
                el.innerHTML = '<div id="layer"><div class="wall wall6" id="wall6_1"></div><div class="wall wall6" id="wall6_2"></div><div class="wall wall6" id="wall6_3"></div><div class="wall wall6" id="wall6_4"></div><div class="wall wall6" id="wall6_5"></div><div class="wall wall6" id="wall6_6"></div><div class="wall wall6" id="wall6_7"></div><div class="wall wall6" id="wall6_8"></div><div class="wall wall6" id="wall6_9"></div><div class="wall wall1" id="wall1_7"></div><div class="wall wall1" id="wall1_8"></div><div class="wall wall1" id="wall1_9"></div><div class="wall wall2" id="wall2_7"></div><div class="wall wall2" id="wall2_8"></div><div class="wall wall2" id="wall2_9"></div><div class="wall wall3" id="wall3_7"></div><div class="wall wall3" id="wall3_8"></div><div class="wall wall3" id="wall3_9"></div><div class="wall wall4" id="wall4_7"></div><div class="wall wall4" id="wall4_8"></div><div class="wall wall4" id="wall4_9"></div></div><div class="wall wall1" id="wall1_1"></div><div class="wall wall1" id="wall1_2"></div><div class="wall wall1" id="wall1_3"></div><div class="wall wall1" id="wall1_4"></div><div class="wall wall1" id="wall1_5"></div><div class="wall wall1" id="wall1_6"></div><div class="wall wall2" id="wall2_1"></div><div class="wall wall2" id="wall2_2"></div><div class="wall wall2" id="wall2_3"></div><div class="wall wall2" id="wall2_4"></div><div class="wall wall2" id="wall2_5"></div><div class="wall wall2" id="wall2_6"></div><div class="wall wall3" id="wall3_1"></div><div class="wall wall3" id="wall3_2"></div><div class="wall wall3" id="wall3_3"></div><div class="wall wall3" id="wall3_4"></div><div class="wall wall3" id="wall3_5"></div><div class="wall wall3" id="wall3_6"></div><div class="wall wall4" id="wall4_1"></div><div class="wall wall4" id="wall4_2"></div><div class=wall "wall4" id="wall4_3"></div><div class="wall wall4" id="wall4_4"></div><div class="wall wall4" id="wall4_5"></div><div class="wall wall4" id="wall4_6"></div><div class="wall wall5" id="wall5_1"></div><div class="wall wall5" id="wall5_2"></div><div class="wall wall5" id="wall5_3"></div><div class="wall wall5" id="wall5_4"></div><div class="wall wall5" id="wall5_5"></div><div class="wall wall5" id="wall5_6"></div><div class="wall wall5" id="wall5_7"></div><div class="wall wall5" id="wall5_8"></div><div class="wall wall5" id="wall5_9"></div>';

                var el2 = document.getElementById('layer');
                window.setTimeout(function () {
                    el2.style.animationName = "obrotDp";
                }, 1);
            }
                break;
        }
    }

}());