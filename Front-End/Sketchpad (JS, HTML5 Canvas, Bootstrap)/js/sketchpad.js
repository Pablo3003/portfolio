(function() {

if(!document.createElement("canvas").getContext) return;

var paint = {

    init: function() {

        //------------- get all needed DOM elements----------------------------//
        this.sketch = document.querySelector("#paint");
        this.colors = this.sketch.querySelectorAll(".colors div");
        this.penSize = this.sketch.querySelector("input[type='range']");
        this.rangeOutput = this.sketch.querySelector("output strong");
        this.canvasContainer = this.sketch.querySelector(".canvas");
        this.canvas = this.sketch.querySelector("canvas");
        this.clearButton = this.sketch.querySelector("#clear");
        this.clearButton.onclick = this.clear.bind(this);

        this.initSidebar();     // setup Siedebar
        this.initCanvas();      // setup canvas element

    },

    //------------------------ Setup canvas element----------------------------//
    initCanvas: function() {

        this.canvas.width = this.canvasContainer.offsetWidth;           // get canvas width the same as parent
        this.canvas.height = this.canvasContainer.offsetHeight;         // get canvas height the same as parent
        this.mouseDown = false;                                         // flag

        this.ctx = this.canvas.getContext("2d");                        // get canvas context

        this.ctx.fillStyle = "white";                                   // canvas background color
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height); // canvas init
        this.ctx.lineWidth = this.penSize.value;                        // set drawing line width
        this.ctx.strokeStyle = this.sketch.querySelector(".current").dataset.color;     // set default color of line

        //-----------------assigning events---------------------------------------//
        this.canvas.onmousemove = this.draw.bind(this);
        this.canvas.onmousedown = this.drawingPossible.bind(this);
        this.canvas.onmouseup = this.drawingImpossible.bind(this);

    },

    //------------------------ Setup sidebar element----------------------------//
    initSidebar: function() {

        [].forEach.call(this.colors, function(color) {                  // as colors is array without method for each
            color.style.backgroundColor = color.dataset.color;          // setting background color same as in data object
            color.onclick = this.colorChange.bind(this);                // assigning onclick event to all color divs
        }.bind(this));

        this.penSize.onchange = function(e) {
            this.rangeOutput.innerHTML = e.target.value;                // change size value on the website
            this.changePenSize(e.target.value);
        }.bind(this);

    },
    //------------------------ method unblocking drawing on mouse key down----------------------------//
    drawingPossible: function(e) {
        this.mouseDown = true;
        this.ctx.beginPath();
        this.ctx.moveTo(this.getX(e), this.getY(e));
    },
    //------------------------ method blocking drawing on mouse key up----------------------------//
    drawingImpossible: function(e) {
        this.mouseDown = false;
    },

    //-------------------------drawing line from x to y on every event-------------------------------------------//
    draw: function(e) {
        if(!this.mouseDown) return;

        var x = this.getX(e);
        var y = this.getY(e);
        this.ctx.lineTo(x, y);
        this.ctx.stroke();

    },

    //-------------------------get x coordinate of mouse pointer when key down-------------------------------------------//
    getX: function(e) {

        var boundries = this.canvas.getBoundingClientRect();

        if(e.offsetX) {         // for browsers with support
            return e.offsetX;
        } else if(e.clientX) {  // if browser window does not have offsetX object
            return e.clientX - boundries.left;
        }

    },


    //-------------------------get y coordinate of mouse pointer when key down-------------------------------------------//
    getY: function(e) {

        var boundries = this.canvas.getBoundingClientRect();

        if(e.offsetY) {
            return e.offsetY;
        } else if(e.clientY) {
            return e.clientY - boundries.top;
        }

    },

    //-------------------------changing color on sidebar and drawing-------------------------------------------//
    colorChange: function(e) {

        this.sketch.querySelector(".current").classList.remove("current");
        e.target.classList.add("current");

        this.ctx.strokeStyle = e.target.dataset.color;

    },

    changePenSize: function(penSize) {

        this.ctx.lineWidth = penSize;

    },

    //-------------------------change pen size-------------------------------------------//
    clear: function() {

        this.ctx.fillStyle = "white"
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

    }
};

paint.init();

})();